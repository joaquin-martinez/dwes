package cliente;

import java.util.logging.Level;
import java.util.logging.Logger;
import utilidades.RecepcionPorConsola;
import utilidades.TransmisionesRecepciones;

/**
 * Clase encargada de pedir la pregunta del cliente, la transmisión de la misma,
 * la recepción de la contestación y su impresion por consola.
 * @author joaquin martinez
 */
public class SiriPunCliente {
    
    private static final String PETICIONDATOS = "TECLEE LA PREGUNTA: " ;
    TransmisionesRecepciones comunicacionesCliente ;
    RecepcionPorConsola obtenerPreguntaUsuario ;
    String respuesta ;
    String contestacion ;
    
    /**
     * Constructor que monta la recepcion del usuario y las transmisiones.
     */
    public SiriPunCliente(){
        comunicacionesCliente = new TransmisionesRecepciones() ;
        obtenerPreguntaUsuario = new RecepcionPorConsola(PETICIONDATOS) ;
    }
    
    /**
     * Ejecuta el guión de la clase.
     */
    public void ejecutar(){
        
        respuesta = obtenerPreguntaUsuario.recepciona() ;
        Logger.getLogger("loger").log(Level.INFO , "el usuario escribio: " + respuesta);
        comunicacionesCliente.ClienteTransmite(respuesta);
        contestacion = comunicacionesCliente.clienteRecibe() ;
        Logger.getLogger("loger").log(Level.INFO, "recibida respuesta " + contestacion );
        comunicacionesCliente.cerrarConector();
        System.out.println( contestacion );
        
    }
    
}
