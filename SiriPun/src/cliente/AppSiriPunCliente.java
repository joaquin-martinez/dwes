package cliente;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import utilidades.SiriPunException;

/**
 * Clase iniciadora de la aplicación cliente de SiriPun.
 * Instancia la clase que conecta con un servidor para enviarle unos datos 
 * recogidos por teclado y recepciona la respuesta, para mostrarla por terminal.
 * Se instancia un log por consola y fichero.
 * @author joaquin martinez.
 */
public class AppSiriPunCliente {
    
private static final Logger LOG_CLIENTE = Logger.getLogger("loger");

    /**
     * Método main.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        FileHandler fh;
        SiriPunCliente siriPunCliente ;
        
            // Preparando configuracion del Logger.
            try {
            fh = new FileHandler( "logCliente.log", true); 
            fh.setLevel(Level.ALL); 
            fh.setFormatter(new SimpleFormatter()); 

            LOG_CLIENTE.addHandler(fh);
            LOG_CLIENTE.setLevel( Level.ALL );
                    

        } catch (IOException ex) {
            System.out.println( "Problemas con fichero de logs. " );
            ex.printStackTrace();
        } catch (SecurityException ex) {
            System.out.println( "Fallo en la configuración del logger. " );
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println( "Fallo indeterminado en la configuración del logger. " );
            ex.printStackTrace();
        }
  
        try{
           siriPunCliente = new SiriPunCliente();
           siriPunCliente.ejecutar();
           
       } catch (SiriPunException spe) {
           System.out.println( "Salimos de la aplicacion: " );
           Throwable causa = spe.getCause();
           causa.getMessage() ;
           causa.printStackTrace();
            
       } catch (Exception ex) {
               System.out.println( "Salida por excepción indeterminada: " );
               System.out.println( ex.getMessage() );
               ex.printStackTrace();
       }       
    }
    
}
