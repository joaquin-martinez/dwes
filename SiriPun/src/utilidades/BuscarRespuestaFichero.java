package utilidades;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/**
 * Clase encargada de buscar en el fichero de preguntas-respuestas, 
 * la respuesta a la pregunta hecha por el usuario.
 * @author joaquin martinez
 */
public class BuscarRespuestaFichero {
    
     Scanner sc ;
     File ficheroDatos ;

     /**
      * Constructor de la clase que recoge el fichero de datos.
      * @param ficheroDatos 
      */
    public BuscarRespuestaFichero(File ficheroDatos) {

        this.ficheroDatos = ficheroDatos ;
    }
    
   public synchronized String buscar( String frase ){
       
       Logger.getLogger("loger").log(Level.INFO, "entramos en buscar." + frase + " en " + ficheroDatos.getName() );
       
       String linea = null ;
       String cadena ;
       
       cadena = limpiaCadena(frase);
       Logger.getLogger("loger").log(Level.INFO, "entramos en buscar." + cadena + " en " + ficheroDatos.getName() );
       
        try {
             sc = new Scanner(new BufferedReader(new FileReader(ficheroDatos )));
        } catch (FileNotFoundException ex) {
             Logger.getLogger("loger").log(Level.SEVERE , "No encontrado el fichero soluciones.dat", ex);
         }
       
       while(sc.hasNext() && linea == null ){
           
            try{
            linea= sc.skip(".*" + cadena + ".*,").nextLine() ;
            }catch(Exception e ){
             sc.nextLine() ;   
            }
           Logger.getLogger("loger").log(Level.INFO, linea);
          
       }
        if(linea == null)
            linea = "Pregunta no contemplada." ;
        
        sc.close();
        
        return linea ;
   }
   
    /**
    * Función encargada de limpiar la pregunta del usuario de caracteres extraños, 
    * y dejar solo un espacio en blanco entre palabras, para que sea mas facil 
    * la comparación con las preguntas en el fichero.
    * @param frase
    * @return 
    */
   private String limpiaCadena(String frase){
       
       String cadena ;
       String[] listaParcial ;
       
       frase = frase.toLowerCase() ;
       frase = frase.trim() ;
       listaParcial = frase.split(" +|[^a-zñ0-9]") ;

       cadena = Arrays.stream(listaParcial).filter(s->s.length()>0).collect(Collectors.joining(" "));

       Logger.getLogger("loger").log(Level.INFO , "limpiada cadena; " + cadena );

       return cadena ;
   }
   
}