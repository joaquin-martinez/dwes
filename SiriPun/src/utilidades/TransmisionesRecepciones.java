package utilidades;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase encargada de las comunicaciones entre cliente y servidor a la escucha.
 * Implementa los métodos para enviar y recibir cadenas mediante sockets tanto en 
 * cliente como en servidor.
 * @author joaquin-martinez.
 */
public class TransmisionesRecepciones {
    
    private static final int PUERTO = 6000 ;
    private static final String SERVIDOR = "localhost" ;
    
    InetSocketAddress direccionServidor ;
    Socket conectorCliente ;
    DataOutputStream flujoSalida ;
    DataInputStream flujoEntrada ;
    DataInputStream flujoEntrada2 ;
    ServerSocket serverSocket ;
    boolean conectado = false ;
    
    /**
     * Constructor de la clase.
     */
    public TransmisionesRecepciones(){
      
        direccionServidor = new InetSocketAddress(SERVIDOR, PUERTO) ;
        conectorCliente = new Socket();

        
    }
    
    /**
     * Método para el envio de cadenas desde el cliente.
     * @param cadena 
     */
    public void ClienteTransmite( String cadena ){
        
        conectarCliente();
        transmitir( cadena );
        
    }
    
    /**
     * Método para la recepción de cadenas desde el cliente.
     * @return 
     */
    public String clienteRecibe(){
        String mensaje ;
        
        conectarCliente();
        mensaje = recibirMensaje() ;
        return mensaje ;
        
    }

    /**
     * Método para el envio de cadenas desde el servidor.
     * @param cadena
     * @param conectorCliente 
     */
    public void servidorTransmite(String cadena , Socket conectorCliente ){
        this.conectorCliente = conectorCliente ;
        transmitir(cadena);
        
        
    }
    
    /**
     * Método para la recepción de cadenas desde el servidor.
     * @param conectorCliente
     * @return 
     */
    public String servidorRecibe( Socket conectorCliente ){
        this.conectorCliente = conectorCliente ;
        String mensaje ;
        
        mensaje = recibirMensaje() ;
        
        return mensaje ;
    }
    
    /**
     * Método para cerrar los flujos.
     * Debe usarse siempre al final.
     */
    public void cerrarConector(){
        try {

            flujoSalida.close();
            flujoEntrada.close();
            conectorCliente.close();
        } catch (IOException ex) {
            Logger.getLogger("loger").log(Level.SEVERE, "Problema con el cierre de conexiones.", ex);
        }
    }
    
    /**
     * Metodo que conecta el cliente con el servidor si no está ya conectado.
     */
    private void conectarCliente(){
        
        if(conectado == false){
        
        try {
            conectorCliente.connect(direccionServidor);
            conectado = true ;
        } catch (IOException ex) {
            Logger.getLogger("loger").log(Level.SEVERE, "Problema en la conexion del cliente al servidor", ex);
        }
        
        }
    }

    /**
     * Método interno de transmision de cadenas usado tanto por el cliente como por el servidor
     * en sus métodos específicos.
     * @param cadena 
     */
    private void transmitir(String cadena ) {

        
        try {
            
            flujoSalida = new DataOutputStream(conectorCliente.getOutputStream()) ;
            flujoSalida.writeUTF(cadena);
        } catch (IOException ex) {
            Logger.getLogger("loger").log(Level.SEVERE, "Problemas de escritura entre cliente/servidor. " , ex);
        }finally {

        }
        


    }
    
    /**
     * Método interno de recepción de cadenas usado tanto por el cliente como por el servidor
     * en sus métodos específicos.
     * @return 
     */
    private String recibirMensaje(){
        
        String respuesta = "" ;        
        try {
            flujoEntrada = new DataInputStream(conectorCliente.getInputStream());
            respuesta = flujoEntrada.readUTF();
            Logger.getLogger("loger").log(Level.INFO, respuesta);
            
        } catch (IOException ex) {
            Logger.getLogger("loger").log(Level.SEVERE, "Problemas de lectura entre cliente/servidor. " , ex);
        } finally {

        }
        
        return respuesta ;
        
    }
    
    
    
    
}
