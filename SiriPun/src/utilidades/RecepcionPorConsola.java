
package utilidades;

import java.util.Scanner;

/**
 * Clase encargada de la recepcion de datos por la consola hasta
 * que se halla introducido el interrogante final (?).
 * @author Joaquin Martinez
 */
public class RecepcionPorConsola {
    
    Scanner leeRespuesta ;
    String peticionDatos ;
    
    public RecepcionPorConsola( String peticionDatos ){
        
        leeRespuesta = new Scanner(System.in); 
        this.peticionDatos = peticionDatos ;
        
    }
    
    /**
     * Metodo que ejecuta el propósito de la clase.
     * @return 
     */
    public String recepciona(){
        
        String respuestaUsuario = "" ;
        
        System.out.println(peticionDatos + " Termine siempre con la interrogación ?. ");
        respuestaUsuario = leeRespuesta.useDelimiter("\\?").next() ;
        leeRespuesta.close();
        return respuestaUsuario ;
       
    }
    
}
