package utilidades;

/**
 * Excepciones de la aplicación no necesariamente capturables.
 * @author Joaquin Martinez.
 */
public class SiriPunException extends RuntimeException {
    
}
