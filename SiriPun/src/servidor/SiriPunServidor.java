package servidor;


import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilidades.BuscarRespuestaFichero;
import utilidades.SiriPunException;

/**
 * Clase encargada de poner el servidor a la escucha y que cuando reciba una peticion 
 * del cliente, la desvie a un hilo encargado de dar la respuesta.
 * Instancia además la clase común encargada de buscar las respuestas en el fichero.
 * Al hilo se le pasaran el socket y el buscador para que pueda cumplir su cometido.
 * @author joaquin martinez
 */
public class SiriPunServidor {

    private static final int PUERTO = 6000;
    private static final String FICHERO = "soluciones.dat" ;
    ServerSocket serverSocket ;
    BuscarRespuestaFichero buscaRespuesta ;
    File ficheroDatos ;
    
    /**
     * Constructor de la clase.
     * Instancia el fichero y el buscador de las respuestas. 
     */
    public SiriPunServidor(){
//        serverSocket = new ServerSocket(PUERTO);
        ficheroDatos = new File(FICHERO) ;
        if (ficheroDatos.exists() && ficheroDatos.canRead()){
        buscaRespuesta = new BuscarRespuestaFichero(ficheroDatos) ;
        Logger.getLogger("loger").log(Level.INFO ,"fichero datos:  " + ficheroDatos.getName());
        }
    }

    /**
     * Ejecuta el propósito de la clase.
     */
    public void ejecutar() {
        try {
            serverSocket = new ServerSocket(PUERTO);
            System.out.println("SERVIDOR: Escuchando por el puerto " + PUERTO
                    + " ...");

            while (true) {
                Socket clientSocket = serverSocket.accept();

                HiloSiriPun hilo = new HiloSiriPun( clientSocket , buscaRespuesta );
                hilo.start();
            }
        } catch (IOException e) {
            Logger.getLogger("loger").log( Level.SEVERE , "Problemas en la creacion del hilo." , e );
            SiriPunException excepSiriPun = new SiriPunException() ;
            excepSiriPun.initCause( e ) ;
            throw excepSiriPun ;

        }
    }
}
