package servidor;

import java.net.Socket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilidades.BuscarRespuestaFichero;
import utilidades.TransmisionesRecepciones;

/**
 * Hilo que gestiona una determinada conexion.
 * Recibe la informacion del usuario mientras está leyendo la información de un fichero por otro hilo.
 * Procesa los datos según estas dos entradas, elaborando una respuesta.
 * Envia la respuesta y cierra las conexiones.
 * @author Joaquin Martinez.
 */
public class HiloSiriPun extends Thread {
    
    Socket conexionCliente ;
    TransmisionesRecepciones comunicacionServidor ;
    String pregunta ;
    BuscarRespuestaFichero buscaRespuesta ;

    LinkedList<String> listaDatos ;

    String respuesta ;
    
    /**
     * Constructor del hilo principal del proceso, a patir del socket de la conexión
     * y de la referencia al controlador del fichero de datos.
     * @param conexionCliente
     * @param leeFichero 
     */
    public HiloSiriPun( Socket conexionCliente , BuscarRespuestaFichero buscaRespuesta ){
        
        this.buscaRespuesta = buscaRespuesta ;
        this.conexionCliente = conexionCliente ;
        this.comunicacionServidor = new TransmisionesRecepciones() ;
        listaDatos = new LinkedList<>() ;

        
    }
    
    /**
     * Desarrollo del método heredado ejecutor.
     */
    @Override
    public void run(){
/*        
        hiloDatos = new HiloDatos( leeFichero , listaDatos ) ;
        hiloDatos.start();
 */       
        pregunta = comunicacionServidor.servidorRecibe(conexionCliente) ;
         Logger.getLogger("loger").log(Level.INFO, pregunta );
       
/*        
        while(hiloDatos.isAlive()){        
        }
  
        respuesta = comparadorContesta.responder(pregunta , listaDatos) ;
*/

        respuesta = buscaRespuesta.buscar(pregunta);
        Logger.getLogger("loger").log(Level.INFO, respuesta );
        comunicacionServidor.servidorTransmite(respuesta, conexionCliente);
        comunicacionServidor.cerrarConector();
        
    }
    
}
