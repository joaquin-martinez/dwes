package servidor;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import utilidades.SiriPunException;

/**
 * Clase iniciadora de la aplicacion servidora de SiriPun.
 * Instancia la clase encargada de ponerse en escucha y aceptar las peticiones
 * cliente para que sean atendidas en un hilo independiente.
 * 
 * Se instancia un log por consola y fichero.
 * @author Joaquin Martinez.
 */
public class AppSiriPunServidor {

    private static final Logger LOG_SERVIDOR = Logger.getLogger("loger");
     
    /**
     * Método main
     * @param args the command line arguments
     */    
    public static void main(String[] args){
        
        SiriPunServidor servidor ;
        FileHandler fh;
        
                    // Preparando configuracion del Logger.
        try {
            fh = new FileHandler( "logServidor.log", true); 
            fh.setLevel(Level.ALL); 
            fh.setFormatter(new SimpleFormatter()); 

            LOG_SERVIDOR.addHandler(fh);
            LOG_SERVIDOR.setLevel( Level.ALL );

        } catch (IOException ex) {
            System.out.println( "Problemas con fichero de logs. " );
            ex.printStackTrace();
        } catch (SecurityException ex) {
            System.out.println( "Fallo en la configuración del logger. " );
            ex.printStackTrace();
        }
 
       // Iniciamos aplicativo. 
       try{
        servidor = new SiriPunServidor() ;
        servidor.ejecutar();
        
        } catch (SiriPunException spe) {
           System.out.println( "Salimos de la aplicacion: " );
           Throwable causa = spe.getCause();
           causa.getMessage() ;
           causa.printStackTrace();
      
       } catch (Exception ex) {
           System.out.println( "Salida por excepción indeterminada: " );
           System.out.println( ex.getMessage() );
           ex.printStackTrace();
       }
    }
    
}
