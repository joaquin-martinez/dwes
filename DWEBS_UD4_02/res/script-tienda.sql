DROP DATABASE IF EXISTS tienda;
CREATE DATABASE tienda CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE tienda;

-- TABLAS
DROP TABLE IF EXISTS articulo;
CREATE TABLE articulo (
	id int(10) NOT NULL,
	codigo varchar(30) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(50) NOT NULL,
	existencia double NOT NULL,
	precio double NOT NULL,
	PRIMARY KEY (id)
);


-- DATOS
INSERT INTO articulo (id, codigo, nombre, descripcion, existencia, precio) VALUES
(5, 'EC001', 'ESFERO ROJO', 'ESFERO BORRABLE', 40, 0.65),
(4, 'EC002', 'ESFERO NEGRO', 'ESFERO BORRABLE', 30, 0.65),
(6, 'FA001', 'FOLDER ARCHIVADOR', 'FOLDER CARTON', 10, 2.79),
(7, 'SM001', 'SOBRE MANILA ', 'SOBRE MANILA OFICIO', 15, 0.1);