package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import dao.DAOImpl;
import model.Articulo;
import utils.Utilities;

/**
 * Servlet implementation class Tienda
 * 
 * Acceso a partir de un DAO por sesi�n
 * 
 */
@WebServlet("/tienda")
public class Tienda extends HttpServlet {
	private final Logger logTag = Logger.getLogger("dwebs");

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DAO dao = getDAO(request.getSession()); // Obtener DAO de la sesi�n
		boolean valueReturn = false; // Valor de retorno en cada modificaci�n del modelo

		response.setContentType(Utilities.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		out.println(Utilities.headWithTitle("Listado de Art�culos"));
		out.println("<body>");

		// Obtener la acci�n
		String action = request.getParameter("action");
		logTag.log(Level.INFO, "la acci�n consultada es: " + action);

		if (action != null) {
			Integer id = Integer.valueOf(request.getParameter("id"));

			if (action.equals("insertar") || action.equals("actualizar")) {
				// 1. Crear el art�culo a partir del paso de par�metros
				String codigo = request.getParameter("codigo");
				String nombre = request.getParameter("nombre");
				String descripcion = request.getParameter("descripcion");
				Double existencia = Double.valueOf(request.getParameter("existencia"));
				Double precio = Double.valueOf(request.getParameter("precio"));

				// 2. Crear el objeto Articulo
				Articulo articulo = new Articulo(id, codigo, nombre, descripcion, existencia, precio);

				// 3. Insertar o actualizar el objeto art�culo en el modelo
				if (action.equals("insertar")) {
					valueReturn = dao.insertar(articulo);

					if (valueReturn)
						out.println("Producto insertado con ID: " + id);
					else
						out.println("Error al insertar");
				} else if (action.equals("actualizar")) {
					valueReturn = dao.actualizar(articulo);

					if (valueReturn)
						out.println("Producto actualizado con ID: " + id);
					else
						out.println("Error al actualizar");
				}

			} else if (action.equals("eliminar")) {
				valueReturn = dao.eliminar(id);

				if (valueReturn)
					out.println("Producto eliminado con ID: " + id);
				else
					out.println("Error al eliminar");
			}
		}

		// LISTADO
		out.println("<table border='1'>");

		out.println("<tr>");
		out.println("<td>ID</td>");
		out.println("<td>C�DIGO</td>");
		out.println("<td>NOMBRE</td>");
		out.println("<td>DESCRIPCI�N</td>");
		out.println("<td>EXISTENCIA</td>");
		out.println("<td>PRECIO</td>");
		out.println("<td>ACCI�N</td>");
		out.println("</tr>");

		List<Articulo> ar = dao.listarArticulos();

		for (Articulo a : ar) {
			out.println("<tr>");
			out.println("<td>" + a.getId() + "</td>");
			out.println("<td>" + a.getCodigo() + "</td>");
			out.println("<td>" + a.getNombre() + "</td>");
			out.println("<td>" + a.getDescripcion() + "</td>");
			out.println("<td>" + a.getExistencia() + "</td>");
			out.println("<td>" + a.getPrecio() + "</td>");
			out.println("<td>" + "<a href='tienda?action=eliminar&id=" + a.getId()
					+ "' title='eliminar'><img src='img/delete.png' alt='eliminar'/></a>" + "</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		// LISTADO

		out.println("</body>");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada y par�metros
	 * inicializados en el descriptor de despliegue
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO
	 */
	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de par�metros del descriptor web.xml
			String driver = getServletContext().getInitParameter("driver");
			String url = getServletContext().getInitParameter("url");
			String bd = getServletContext().getInitParameter("bd");
			String username = getServletContext().getInitParameter("username");
			String password = getServletContext().getInitParameter("password");

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", new DAOImpl(driver, url, bd, username, password));

			// 3. Nuevo DAO generado para la sesi�n
			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}
}