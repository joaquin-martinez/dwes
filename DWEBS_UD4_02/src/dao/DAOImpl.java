package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import model.Articulo;
import utils.Conexion;

public class DAOImpl implements DAO {
	private static final Logger logTag = Logger.getLogger(DAOImpl.class.getName());
	private Conexion conexion;
	
	public DAOImpl(String driver, String url, String bd, String username, String password) {
		this.conexion = new Conexion(driver, url, bd, username, password);
	}

	public DAOImpl(Properties props) {
		this.conexion = new Conexion(props);
	}

	@Override
	public boolean insertar(Articulo articulo) {
		Connection c;
		boolean valueReturn = false;
		String sql = "INSERT INTO articulo (id, codigo, nombre, descripcion, existencia, precio) VALUES (?, ?, ?, ?, ?, ?)";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, articulo.getId());
			statement.setString(2, articulo.getCodigo());
			statement.setString(3, articulo.getNombre());
			statement.setString(4, articulo.getDescripcion());
			statement.setDouble(5, articulo.getExistencia());
			statement.setDouble(6, articulo.getPrecio());

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al insertar: " + e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public List<Articulo> listarArticulos() {
		Connection c;
		List<Articulo> listaArticulos = new ArrayList<Articulo>();
		String sql = "SELECT * FROM articulo";

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Consultar campos
				int id = resulSet.getInt("id");
				String codigo = resulSet.getString("codigo");
				String nombre = resulSet.getString("nombre");
				String descripcion = resulSet.getString("descripcion");
				Double existencia = resulSet.getDouble("existencia");
				Double precio = resulSet.getDouble("precio");

				// Crear objeto Articulo
				Articulo articulo = new Articulo(id, codigo, nombre, descripcion, existencia, precio);

				// Insertar objeto en colecci�n
				listaArticulos.add(articulo);
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaArticulos;
	}

	@Override
	public Articulo obtenerPorId(int id) {
		Connection c;
		Articulo articulo = null;
		String sql = "SELECT * FROM articulo WHERE id= ? ORDER BY id";
		boolean encontrado = false;

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, id);

			ResultSet res = statement.executeQuery();
			if (res.next() && !encontrado) {
				if (res.getInt("id") == id) {
					articulo = new Articulo(res.getInt("id"), res.getString("codigo"), res.getString("nombre"),
							res.getString("descripcion"), res.getDouble("existencia"), res.getDouble("precio"));
					encontrado = true;
				}
			}

			// Cierre de conexiones
			res.close();
			this.conexion.desconectar();

		} catch (SQLException e) {
			logTag.severe("Error en la obtenci�n por ID: " + e.getMessage());
		}

		return articulo;
	}

	@Override
	public boolean actualizar(Articulo articulo) {
		Connection c;
		boolean valueReturn = false;
		String sql = "UPDATE articulo SET codigo=?,nombre=?,descripcion=?,existencia=?, precio=? WHERE id=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setString(1, articulo.getCodigo());
			statement.setString(2, articulo.getNombre());
			statement.setString(3, articulo.getDescripcion());
			statement.setDouble(4, articulo.getExistencia());
			statement.setDouble(5, articulo.getPrecio());
			statement.setInt(6, articulo.getId());

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al actualizar: "+ e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public boolean eliminar(int id) {
		Connection c;
		boolean valueReturn = false;
		String sql = "DELETE FROM articulo WHERE id=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, id);

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al eliminar: " + e.getMessage());
		}

		return valueReturn;
	}
}
