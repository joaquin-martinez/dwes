package dao;

import java.util.List;

import model.Articulo;

public interface DAO {
	public boolean insertar(Articulo articulo);
	public List<Articulo> listarArticulos();
	public Articulo obtenerPorId(int id);
	public boolean actualizar(Articulo articulo);
	public boolean eliminar(int id);
}
