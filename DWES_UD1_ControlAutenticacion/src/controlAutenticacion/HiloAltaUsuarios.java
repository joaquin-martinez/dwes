package controlAutenticacion;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HiloAltaUsuarios extends Thread {

    private Socket clientSocket = null;
 //   private ObjectOutputStream flujoSalida;

    public HiloAltaUsuarios(Socket cliente) throws IOException {
        this.clientSocket = cliente;
 //       flujoSalida = new ObjectOutputStream(cliente.getOutputStream());
    }

    public void run() {
        
        
                    // 1. Crear flujo de SALIDA hacia el cliente (envío del OBJETO)
            ObjectOutputStream flujoSalida;
        try {
            flujoSalida = new ObjectOutputStream(
                    clientSocket.getOutputStream());


            // 2. ENVIO del OBJETO hacia el cliente
            // Se crea un usuario con nuevo password
            Usuario user = new Usuario("paquito", GeneradorPassword.generarPassword());
            System.out
                    .println("SERVIDOR: Un cliente se ha conectado y sus nuevas credenciales de acceso son: "
                    + user.getUsername() + " - " + user.getPassword());
            flujoSalida.writeObject(user);

            // 3. Crear flujo de ENTRADA del cliente (recepción del OBJETO)
            ObjectInputStream flujoEntrada = new ObjectInputStream(
                    clientSocket.getInputStream());

            // 4. RECEPCIÓN del OBJETO del cliente
            Usuario newUser;
                try {
                    newUser = (Usuario) flujoEntrada.readObject();

            System.out.print("SERVIDOR: NUEVAS CREDENCIALES DEL USUARIO: "
                    + newUser.getUsername() + " - " + newUser.getPassword()
                    + " - " + newUser.getIp());
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(HiloAltaUsuarios.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            // 5. Cierre de flujos y conexión del cliente (en orden)
            flujoEntrada.close();
            flujoSalida.close();
            clientSocket.close();

            // 6. Cierre de conexión principal del servidor
//            serverSocket.close();



        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
        
        
        
        
        //////////////
    /*
        try {
            System.out.println("INICIO DE COMUNICACIÓN: "
                    + cliente.getInetAddress().getHostAddress());
            int num = aleatorio(1, 10);
            flujoSalida.writeUTF("ME DORMIRÉ " + num + " SEGUNDOS");

            // Espera simulada de N segundos (observar comportamiento
            // concurrente del servidor cuando genera hilos dedicados)
            Thread.sleep(num * 1000);

            System.out.println("FIN DE COMUNICACIÓN (tras pasar " + num
                    + " segundos): "
                    + cliente.getInetAddress().getHostAddress());

            // Cierre de flujos y conexión (en orden)
            flujoSalida.close();

            // Cierre de conexión del cliente
            cliente.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static int aleatorio(int min, int max) {
        Random r = new Random();
        return min + r.nextInt(max - min + 1); // Entre min y max -> [min,max];
    }
*/
}
