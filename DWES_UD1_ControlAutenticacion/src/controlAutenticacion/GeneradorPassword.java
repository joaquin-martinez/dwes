/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlAutenticacion;

import java.util.Random;

/**
 *
 * @author ara
 */
public class GeneradorPassword {
    
        private static final int LONGITUD_CONTRASENIA = 10; // Longitud máxima de
    // las contraseñas de
    // usuarios

    /* FUNCIÓN DE GENERACIÓN DE CONTRASEÑA PARA LOS NUEVOS USUARIOS */
    public static String generarPassword() {
        char c;
        String password = "";

        for (int i = 0; i < LONGITUD_CONTRASENIA; i++) {
            c = (char) (aleatorio(65, 126));
            password += c;
        }

        return password;
    }

    private static int aleatorio(int min, int max) {
        Random r = new Random();
        return min + r.nextInt(max - min + 1); // Entre min y max -> [min,max];
    }
    
}
