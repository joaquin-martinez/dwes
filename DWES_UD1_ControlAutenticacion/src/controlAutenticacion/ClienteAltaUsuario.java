package controlAutenticacion;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteAltaUsuario {

    private static final int PUERTO = 6000;
    private static final String SERVER = "localhost";

    public static void main(String[] args) throws ClassNotFoundException {
        try {
            // Flujo de entrada para solicitar el usuario
            // por teclado
            Scanner sc = new Scanner(System.in);
            String clave = GeneradorPassword.generarPassword() ;
            Usuario user = new Usuario("usuario" , clave ) ;
            
            HiloPedirNombre hiloPedirNombre = new HiloPedirNombre(user) ;
            hiloPedirNombre.start();

            Socket clientSocket = new Socket();
            InetSocketAddress addr = new InetSocketAddress(SERVER, PUERTO);
            clientSocket.connect(addr);

            // 1. Crear flujo de ENTRADA al servidor (recepción del OBJETO)
            ObjectInputStream flujoEntrada = new ObjectInputStream(
                    clientSocket.getInputStream());

            // 2. RECEPCION del OBJETO del servidor
            Usuario newUser = (Usuario) flujoEntrada.readObject();
            
            while( hiloPedirNombre.isAlive()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClienteAltaUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            newUser.setUsername(user.getUsername());
            
            System.out.println("CLIENTE: Recibiendo del SERVIDOR las nuevas credenciales de acceso: "
                    + newUser.getUsername()
                    + " - "
                    + newUser.getPassword());

            // 3. Crear flujo de SALIDA hacia el servidor (envío del OBJETO)
            ObjectOutputStream flujoSalida = new ObjectOutputStream(
                    clientSocket.getOutputStream());

            // 4. ENVIO del OBJETO hacia el servidor
            System.out.println("Introduce la nueva contraseña del usuario "
                    + newUser.getUsername() + ":");
            String newPassword = sc.nextLine();
            // Se modifican los valores del objeto
            newUser.setPassword(newPassword);
            newUser.setIp(InetAddress.getLocalHost().getHostAddress());
            // Se escribe el objeto
            flujoSalida.writeObject(newUser);

            // 5. Cierre de flujos de datos y conexión (en orden)
            sc.close();
            flujoSalida.close();
            flujoEntrada.close();
            clientSocket.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
