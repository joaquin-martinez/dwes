package modelo;


import java.util.HashMap;

/**
 * Clase que guarda los descriptores de los posibles estados o modos del
 * servidor.
 *
 * @author joaquin martinez
 */
public class ListaEstadosServidor {

    private HashMap<String, String> mapEstadosServidor;

    /**
     * Constructor de la clase que carga los diferentes modos contemplados y sus
     * descriptores.
     */
    public ListaEstadosServidor() {
        mapEstadosServidor = new HashMap<String, String>();
        mapEstadosServidor.put("d", "Diurno");
        mapEstadosServidor.put("m", "Mantenimiento");
        mapEstadosServidor.put("n", "Nocturno");
    }

    /**
     * Método que devuelve el descriptor del modo.
     *
     * @param clave
     * @return
     */
    public String getvalor(String clave) {
        return mapEstadosServidor.get(clave);
    }

}
