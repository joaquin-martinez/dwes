package modelo;

/**
 * Clase que define cada estado o modo en el que trabaka el servidor.
 *
 * @author joaquin martinez
 */
public class EstadoServidor {

	private String modo;
	private Integer horaInicio;

	/**
	 * Constructor del modo segun cadena de parametro de contexto.
	 *
	 * @param nuevoModo
	 */
	public EstadoServidor(String nuevoModo) {
		modo = nuevoModo.substring(0, 1);
		try {
			horaInicio = Integer.parseInt(nuevoModo.substring(1));
		} catch (NumberFormatException nfe) {
			throw new RuntimeException(
					"Mal formateado del modo en web.xml, letra + hora en" + " formato 24h y separado por guiones.",
					nfe);
		}

		if (!modo.matches("[dmn]")) {
			throw new RuntimeException(
					"Modo no contemplado." + "Posiblemente esté mal la variable de contexto o no se halla "
							+ "definido como estado del servidor.");
		}
	}

	/**
	 * Método que devuelve la hora de inicio del modo.
	 *
	 * @return
	 */
	public int getHoraInicio() {
		return horaInicio;
	}

	/**
	 * Método que devuelve el modo del estado del servidor.
	 *
	 * @return
	 */
	public String getModo() {
		return modo;
	}
}
