package modelo;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Clase encargada de informar del estado o modo en el que se encuentra el
 * servidor, segun horarios preestablecidos y la hora del sistema.
 *
 * @author joaquin martinez
 */
public class Mantenimiento {

	private static final Logger log = Logger.getLogger("mantenimiento");
	private int horaInicioEstado = 0;
	private ListaEstadosServidor listadoEstadosServidor; // = new ListaEstadosServidor();
	private LinkedList<EstadoServidor> listaEstados;
	private LocalDateTime diaHora;
	private Respuesta respuesta;

	/**
	 * Constructor de la clase.
	 */
	public Mantenimiento() {
		listadoEstadosServidor = new ListaEstadosServidor();
		listaEstados = new LinkedList<EstadoServidor>();
	}

	/**
	 * M�todo que nos informa del estado del servidor, a partir de los horarios y
	 * la hora actual del servidor.
	 *
	 * @param horarios
	 *            Parametros de los horarios del servidor.
	 * @return  Descripci�n del estado actual del servidor.
	 */
	public Respuesta getEstado(String horarios) {

		String estado = "Fallo en la obtenci&oacute;n del estado.";
		String[] periodos;
		LinkedList<Integer> listaHoraria;
		EstadoServidor estadoActual;
		int indice = 0;
		Integer intHora;

		log.setParent(Logger.getLogger("dayServlet"));
		log.setLevel(Level.ALL);
		periodos = horarios.split("-");
		log.log(Level.INFO, (String) (Arrays.stream(periodos).collect(Collectors.joining(" - "))));
		Arrays.stream(periodos).forEach(x -> {
			EstadoServidor modoServidor = new EstadoServidor(x);
			listaEstados.add(modoServidor);
		});

		listaHoraria = listaEstados.stream().map(x -> x.getHoraInicio())
				.collect(Collectors.toCollection(LinkedList<Integer>::new));

		intHora = getHora();

		if (!listaHoraria.contains(intHora)) {

			listaHoraria.add(intHora);
			listaHoraria.sort((c, z) -> c - z);
			log.log(Level.INFO,
					(String) (listaHoraria.stream().map(x -> Integer.toString(x)).collect(Collectors.joining(" < "))));
			indice = listaHoraria.indexOf(intHora) - 1;
			if (indice < 0) {
				indice = listaHoraria.size() - 1;
			}
		} else {
			indice = listaHoraria.indexOf(intHora);
		}

		horaInicioEstado = listaHoraria.get(indice);

		estadoActual = listaEstados.stream().filter(z -> z.getHoraInicio() == horaInicioEstado).findFirst().get();

		estado = listadoEstadosServidor.getvalor(estadoActual.getModo());

		respuesta = new Respuesta(getLaHora(), estado);

		return respuesta;
	}

	/**
	 * M�todo interno para la inicializacion de la propiedad diaHora y la obtenci�n de intHora.
	 * @return Devuelve la hora actual en formato 0-24, sin los minutos ni segundos en forma de entero.
	 */
	private Integer getHora() {
		Integer intHora;
		diaHora = LocalDateTime.now();
		intHora = diaHora.getHour();

		return intHora;

	}

	/**
	 * M�todo interno para la transformacion de fecha y hora a cadena legible.
	 * @return Cadena con; la hora de la fecha.
	 */
	private String getLaHora() {
		String cadenaDiaHora;
		cadenaDiaHora = diaHora.getHour() + ":" + diaHora.getMinute() + ":" + diaHora.getSecond() + " del "
				+ diaHora.getDayOfMonth() + " - " + diaHora.getMonthValue() + " - " + diaHora.getYear();
		return cadenaDiaHora;
	}

}
