package modelo;

/**
 * Encapsula los datos de la respuesta del modelo.
 * 
 * @author joaquin martinez
 *
 */
public class Respuesta {
	
	private String diaHoraActual;
	private String modo;

	/**
	 * Constructor de la clase que inicia las cadenas solicitadas por los
	 * requerimientos.
	 * 
	 * @param diaHoraActual
	 *            Cadena con la hora y el dia.
	 * @param modo
	 *            Cadena que informa del modo del servidor actualmente.
	 */
	public Respuesta(String horaActual, String modo) {
		this.diaHoraActual = horaActual;
		this.modo = modo;
	}

	/**
	 * M�todo que devuelve la hora del dia actual.
	 * 
	 * @return Cadena con la hora y el dia.
	 */
	public String getDiaHoraActual() {
		return diaHoraActual;
	}

	/**
	 * M�todo que devuelve el modo del servidor seg�n la hora actual.
	 * 
	 * @return Cadena que informa del modo del servidor actualmente.
	 */
	public String getModo() {
		return modo;
	}

}
