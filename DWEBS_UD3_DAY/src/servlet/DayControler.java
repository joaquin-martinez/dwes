package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Mantenimiento;
import modelo.Respuesta;

/**
 * Servlet implementation class DayControler
 */
@WebServlet("/daycontroler")
public class DayControler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger("dayServlet");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DayControler() {
		super();
		log.log(Level.WARNING, "Se construye el servlet ...");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext ctx = this.getServletContext();
		Mantenimiento mantenimiento = new Mantenimiento();
		Respuesta respuesta;
		String horarios = "fallo en la obtencion de horarios.";

		log.log(Level.INFO, "Damos servicio en: " + this.getServletContext().getContextPath());

		log.log(Level.WARNING, "Recivida peticion servicio.");
		PrintWriter out = response.getWriter();
		
		out.println("<h1>Hola mundo desde un servlet.</h1>");
		if (ctx.getInitParameter("appName") != null)
			out.println("<h3>" + ctx.getInitParameter("appName") + "</h3>");
		else {
			out.println("<h2>No se pudo obtener el nombre de la aplicacion.</h2>");
		}
		if (ctx.getInitParameter("ModosServidor") != null)
			horarios = ctx.getInitParameter("ModosServidor");
		else {
			out.println("<h2>No se pudieron obtener los modos del servidor.</h2>");
		}
		
		
		try {
		respuesta = mantenimiento.getEstado(horarios);



		out.println("<h2>Son las: " + respuesta.getDiaHoraActual() + "</h2>");


		if (ctx.getInitParameter("ModosServidor") != null)
			out.println("<p>El servidor est� en modo; " + respuesta.getModo() + "</p>");

		}catch(Exception ex){
			out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
