package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import beans.Libro;
import utilidades.Conexion;

public class DAOImpl implements DAO {
	private static final Logger logTag = Logger.getLogger(DAOImpl.class.getName());
	private Conexion conexion;
	
	public DAOImpl(String driver, String url, String bd, String username, String password) {
		this.conexion = new Conexion(driver, url, bd, username, password);
	}

	public DAOImpl(Properties props) {
		this.conexion = new Conexion(props);
	}

	@Override
	public boolean insertar(Libro libro) {
		Connection c;
		boolean valueReturn = false;
		String sql = "INSERT INTO alojamiento (titulo, autor, fecha) VALUES (?, ?, ?)";
		

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setString(1, libro.getTitulo());
			statement.setString(2, libro.getAutor());
			statement.setString(3, libro.getFecha());


			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al insertar: " + e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public List<Libro> listarLibros() {
		Connection c;
		List<Libro> listaLibro = new ArrayList<Libro>();
		String sql = "SELECT * FROM libro ORDER BY fecha DESC";

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Consultar campos
//				int id = resulSet.getInt("id");
				String titulo = resulSet.getString("titulo");
				String autor = resulSet.getString("autor");
				String fecha = resulSet.getString("fecha");
				

				// Crear objeto Articulo
				Libro libro = new Libro(titulo , autor , fecha);

				// Insertar objeto en colecci�n
				listaLibro.add(libro);
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaLibro;
	}

	/*	
	public Libro obtenerPorId(int id) {
		Connection c;
		Libro libro = null;
		String sql = "SELECT * FROM libro WHERE id= ? ORDER BY referencia";
		boolean encontrado = false;

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, id);

			ResultSet res = statement.executeQuery();
			if (res.next() && !encontrado) {
				if (res.getInt("id") == id) {
					libro = new Libro(res.getInt("id"), res.getString("titulo"), res.getString("autor"),
							res.getString("fecha"));
					encontrado = true;
				}
			}

			// Cierre de conexiones
			res.close();
			this.conexion.desconectar();

		} catch (SQLException e) {
			logTag.severe("Error en la obtenci�n por ID: " + e.getMessage());
		}

		return libro;
	} */

	@Override
	public boolean actualizar(Libro libro) {
		Connection c;
		boolean valueReturn = false;
		String sql = "UPDATE libro SET titulo=?,autor=?,fecha=? WHERE id=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setString(1, libro.getTitulo());
			statement.setString(2, libro.getAutor());
			statement.setString(3, libro.getFecha());
//			statement.setInt(4, libro.getId());

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al actualizar: "+ e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public boolean eliminar(Libro libro) {
		Connection c;
//		int id = libro.getId();
		boolean valueReturn = false;
		String sql = "DELETE FROM libro WHERE id=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
//			statement.setInt(1, id);

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al eliminar: " + e.getMessage());
		}

		return valueReturn;
	}
}
