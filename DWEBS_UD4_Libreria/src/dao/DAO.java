package dao;

import java.util.List;

import beans.Libro;

public interface DAO {
	public boolean insertar(Libro libro);
	public List<Libro> listarLibros();
	public boolean actualizar(Libro libro);
	public boolean eliminar(Libro libro);
	
}
