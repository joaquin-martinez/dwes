package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import dao.DAOImpl;
import serializadores.ProcesadorJAXB;
import utilidades.Utilities;
import beans.*;

/**
 * Servlet implementation class TiendaLibros
 */
@WebServlet("/tiendalibros")
public class TiendaLibros extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logTag = Logger.getLogger("libros");
   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TiendaLibros() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String salida;
	DAO dao ;
	List<Libro> listaLibros;

	PrintWriter out = response.getWriter();
	Libros libros = new Libros();
	HttpSession sesion = request.getSession(); 
	ProcesadorJAXB<Libros> procesador = new ProcesadorJAXB<>();
	
	dao = getDAO(sesion);
	listaLibros =  dao.listarLibros();
	listaLibros.forEach(x-> System.out.println(x.getTitulo()));
	libros.getLibro().addAll(listaLibros);
	if(request.getParameter("accion")==null) {
		salida=procesador.serializarXML(libros);
		response.setContentType(Utilities.CONTENT_TYPE_XML);
	}else {
	salida = procesador.serializarJSON(libros);
	response.setContentType(Utilities.CONTENT_TYPE_JSON);
	}
	System.out.println("La salida es: " + salida);
	out.println(salida);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}



	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada y par�metros
	 * inicializados en el descriptor de despliegue
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO
	 */
	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de par�metros del descriptor web.xml
			String driver = getServletContext().getInitParameter("driver");
			String url = getServletContext().getInitParameter("url");
			String bd = getServletContext().getInitParameter("bd");
			String username = getServletContext().getInitParameter("username");
			String password = getServletContext().getInitParameter("password");

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", new DAOImpl(driver, url, bd, username, password));

			// 3. Nuevo DAO generado para la sesi�n
			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}
	
}
