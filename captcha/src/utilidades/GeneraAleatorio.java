package utilidades;

import java.util.Random;
/**
 * Clase que genera un numero aleatorio entre un minimo y un maximo.
 * @author joaquin
 *
 */
public class GeneraAleatorio {

	public static int aleatorio(int min, int max) {
		Random r = new Random();
		return min + r.nextInt(max - min + 1); // Entre min y max -> [min,max];
	}

}
