package utilidades;

import java.util.logging.Logger;

/**
 * Clase que comprueba que el numero se corresponde con la letra del nif.
 * @author joaquin
 *
 */
public class ValidaNif {
	private static final char[] letras = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z',
			'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E' };
	private static final Logger logNif = Logger.getLogger("logCaptcha");

	public static boolean validar(String nif) {
		boolean valido = false;

		if (nif.matches("[0-9]{7,9}[a-z]")) {

			char letra;
			int numero;
			int modulo;

			letra = nif.toUpperCase().charAt(nif.length() - 1);
			logNif.info("letra " + letra);
			numero = Integer.parseInt(nif.substring(0, nif.length() - 1));
			logNif.info("numero " + numero);
			modulo = numero % 23;
			logNif.info("modulo " + modulo);
			if (letra == letras[modulo]) {
				valido = true;
			}
		}
		logNif.info("valido " + valido);
		return valido;
	}

}
