package utilidades;

public abstract class Utilities {
	public static final String CONTENT_TYPE = "text/html";
	public static final String DOCTYPE = "<!DOCTYPE HTML>";

	public static String headWithTitle(String title) {
		return (DOCTYPE + "\n\r" + "<html>\n\r" + "<head>\n\r<title>" + title
				+ "</title>\n\r<meta charset=\"utf-8\"/><script src= \".\\js\\javascript.js\"> </script>\n\r"
				+ "<link rel=\"stylesheet\" type=\"text/css\" href=\".\\css\\estilos.css\" media=\"screen\" />"
				+ "</head>\n\r");
	}
}
