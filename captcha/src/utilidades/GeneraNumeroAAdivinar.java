package utilidades;
/**
 * Clase que genera n cifras aleatorias y las devuelve como String.
 * @author joaquin
 *
 */
public class GeneraNumeroAAdivinar {
	
	public static String genera(int n) {
		String StringNumero = "";
		
		for(int i =0 ; i<n ; i++) {
			int numero =GeneraAleatorio.aleatorio(0, 9);
			StringNumero += numero;
		}
		
		return StringNumero;
	}

}
