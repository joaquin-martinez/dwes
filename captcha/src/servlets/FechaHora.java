package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilidades.Utilities;

/**
 * Servlet implementacion clase FechaHora, encargado de proporcionar la fecha y
 * hora del servidor para la aplicacion captcha.
 */
@WebServlet("/fechahora")
public class FechaHora extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FechaHora() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String salida;
		LocalDateTime fechaHora = LocalDateTime.now();
		PrintWriter out = response.getWriter();

		salida = fechaHora.getDayOfMonth() + " / " + fechaHora.getMonthValue() + " / " + fechaHora.getYear() + " a las "
				+ fechaHora.getHour() + ":" + fechaHora.getMinute() + " horas.";
		response.setContentType(Utilities.CONTENT_TYPE);
		out.println(salida);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
