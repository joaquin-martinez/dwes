package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.Utilities;
import utilidades.ValidaNif;

/**
 * Servlet implementation class Procesar
 */
@WebServlet({ "/procesos", "/procesar" })
public class Procesar extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logApp = Logger.getLogger("logCaptcha");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Procesar() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nif;
		String lecturaCaptcha;
		String captcha;
		PrintWriter out = response.getWriter();
		boolean nifValido;

		HttpSession sesion = request.getSession();
		nif = request.getParameter("nif");
		nifValido = ValidaNif.validar(nif);
		logApp.info("el nif introducido es: " + nif + " validacion= " + nifValido);
		lecturaCaptcha = request.getParameter("numero");
		logApp.info("el captcha introducido es: " + lecturaCaptcha);
		captcha = (String) sesion.getAttribute("captcha");
		logApp.info("el captcha real es: " + captcha);
		response.setContentType(Utilities.CONTENT_TYPE);
		out.println(nif + " -- " + lecturaCaptcha);
		if (nifValido) {
			out.println("<br> El nif es valido. ");
		} else {
			out.println("<br> El nif no es valido. ");
		}
		if (captcha.equals(lecturaCaptcha)) {
			out.println("<br> El captcha es valido. ");
		} else {
			out.println("<br> El captcha no es valido. ");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
