window.addEventListener("load", inicializar, false);
function inicializar() {
	document.getElementsByTagName("form")[0].setAttribute("onsubmit",
			"return procesar()");

	document.getElementById("recarga").addEventListener("click", nuevoCaptcha);
	nuevoCaptcha();
	obtenFechaHora("fechaHora", "fechahora");

}
function getResponse(action) {
	// ** 1. Definir y concatenar parámetros de envío a partir de los "id" de
	// cada elemento
	// (si hubiera parámetros de envío)
	var URLparams = ""

	// 2. Creación de objeto request
	var xhttp = new XMLHttpRequest();

	// 3. Definición del manejador cuando reciba el contenido asíncrono
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// ** 4. Qué hacer cuando se reciba correctamente el
			// this.responseText
			var html = this.responseText;
			// document.getElementById(id).innerHTML = html;
			document.getElementsByTagName("text")[0].innerHTML = html;
		}
	};

	// 5. Método de envío
	xhttp.open("POST", action, true);

	// 6. Envíar petición
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(URLparams);
}

function nuevoCaptcha() {
	getResponse("captcha");
}

function obtenFechaHora(id, action) {
	// ** 1. Definir y concatenar parámetros de envío a partir de los "id" de
	// cada elemento
	// (si hubiera parámetros de envío)
	var URLparams = "";

	// 2. Creación de objeto request
	var xhttp = new XMLHttpRequest();

	// 3. Definición del manejador cuando reciba el contenido asíncrono
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// ** 4. Qué hacer cuando se reciba correctamente el
			// this.responseText
			var html = this.responseText;
			document.getElementById(id).innerHTML = html;
		}
	};

	// 5. Método de envío
	xhttp.open("POST", action, true);

	// 6. Envíar petición
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(URLparams);
}
function procesar() {

	console.log(nif.value);
	var URLparams = "nif=" + nif.value + "&numero=" + numero.value;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// ** 4. Qué hacer cuando se reciba correctamente el
			// this.responseText
			var html = this.responseText;
			console.log(html);
			document.getElementById("contesta").innerHTML = html;
		}

	};
	xhttp.open("POST", "procesar", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(URLparams);
	return false;
}
