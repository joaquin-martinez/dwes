package utils;

import java.util.logging.Logger;

/**
 * Clase que en cada llamada da un color diferente.
 * Se puede inicializar con un color de partida con formato html hexadecimal.
 * Además. podemos definir un incremento, que se utilizará para sumarlo al color y 
 * así obtener el nuevo color.
 * @author joaquin martinez.
 */
public class CambiaColor {
    
    private static final Logger LogConsola = Logger.getLogger("cambiacolor") ;
    String color ;
    int incremento ;

    /**
     * constructor que inicia la clase a partir de un color expresado en hexadecimal.
     * @param colorInicial 
     */
    public CambiaColor( String colorInicial ){
    this.color = colorInicial ;
    this.incremento = 256 ;
    }

    /**
     * constructor que inicializa la clase con unos valores predeterminados.
     */
    public CambiaColor( ){
        color = "#00ff00" ;
        incremento = 256;
    }
    
    /**
     * Constructor de la clase que recibe un entero , que sumado al color
     * predeterminado, servirá para ir claculando los sucesivos colores distontos.
     * @param incremento Salto en el color, resultado de la diferencia entre
     * el color inicial y el siguiente calculado.
     */
    public CambiaColor( int incremento){
        this.color = "#00ff00" ;
        this.incremento = incremento ;
    }

    
    /**
     * Constructor que inicializa la clase a partir de un color inicial hexadecimal
     * en string, in el incremento que queramos sumarle para clcular el nuevo color
     * a devolver.
     * @param colorInicial color del que partimos.
     * @param intervalo Incremento a sumar al color inicial.
     */
    public CambiaColor( String colorInicial , int intervalo ){
    this.color = colorInicial ;
    this.incremento = intervalo ;
    }

    /**
     * Metodo de la clase que se encarga de devolver un color hexadecimal, sumando
     * cierta cantidad al color anterior.
     * @return 
     */
    public String getNuevoColor(){
        Integer colorHexa ;
        colorHexa = Integer.parseInt((color.split("#"))[1] , 16) ;
        LogConsola.info("Generando nuevo color, a partir de: " + color);
        colorHexa += incremento ;
        color = Integer.toHexString(colorHexa) ;
        color = this.ponerCeros(color);
        color = "#" + color ;
        LogConsola.info("color nuevo en hexadecimal: " + color);
        return color ;
    }
    
    /**
     * Método interno que ajusta el formato hexadecimal de seis dígitos del
     * color añadiendo ceros si es necesario.
     * @param color color hexadecimal en string.
     * @return  Color hexadecimal con 6 digitos,
     */
    private String ponerCeros(String color){
        int nc;
        
        if((nc =6 - color.length())>=0){
           for(int y =0 ; y <nc ; y++){
               color = "0" + color ;
           }
            
        }else color="000000" ;
        
        return color ;
    }
    
}
