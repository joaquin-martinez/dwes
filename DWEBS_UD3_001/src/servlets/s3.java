package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import utils.CambiaColor;
import utils.Utilities;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.istack.internal.logging.Logger;

/**
 * Servlet implementation class s3
 */
@WebServlet(name="s3",urlPatterns= {"/getinfo","/s3"})
public class s3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger ls3 = Logger.getLogger(s3.class) ;
	CambiaColor cambiacolor ;
//	private static final Logger logS3 = Logger.getLogger(s3.class) ;
	String color ;
	String colorFondo = "" ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public s3() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    

	@Override
	public void init() throws ServletException {
		super.init();
		
		if((color=this.getServletConfig().getInitParameter("color"))==null) {
			color = "#0000ff" ;
		}
		cambiacolor = new CambiaColor( color , 0x1010f ) ;

	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(colorFondo.equals(""))
			colorFondo = color ;
		else {
			colorFondo = cambiacolor.getNuevoColor() ;
		}
		
		response.setContentType(Utilities.CONTENT_TYPE);
		PrintWriter out = response.getWriter() ; 
		out.println( Utilities.headWithTitle(this.getServletContext().getInitParameter("appName")));
		out.println("<body bgcolor=\"" + colorFondo + "\" >");
		out.println("<ul>");
		out.println( "<li>IP servidor: " + request.getLocalAddr() + "</li>");
		out.println( "<li>IP Ciente: " + request.getRemoteAddr() + "</li>");
		out.println( "<li>Puerto servidor: " + request.getServerPort() + "</li>");
		out.println("</ul>");
		out.println("</body>");
		out.println("</html>");
		
		out.flush();

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
