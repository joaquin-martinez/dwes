package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class s1
 */
@WebServlet("/fulanito")
public class s1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
 

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LocalDateTime date = LocalDateTime.now() ;
		ServletContext ctx = this.getServletContext() ;
		
		PrintWriter out = response.getWriter() ;
		out.println("Hola mundo desde un servlet.");
		out.println("Son las: " + date.getHour() + ":" + date.getMinute() + ":" + date.getSecond() );
		if(ctx.getInitParameter("appName")!= null )
		out.println(ctx.getInitParameter("appName")) ;
		if(ctx.getInitParameter("Diurno")!= null ) {
		out.println("Diurno") ;	
		out.println(ctx.getInitParameter("Diurno")) ;
		String horarioDiurno = ctx.getInitParameter("Diurno");
		}
		if(ctx.getInitParameter("En mantenimiento")!= null ) {
		out.println("En mantenimiento") ;	
		out.println(ctx.getInitParameter("En mantenimiento")) ;
		String horarioMantenimiento = ctx.getInitParameter("En mantenimiento");
		}
		if(ctx.getInitParameter("Nocturno")!= null ) {
		out.println("Nocturno") ;	
		out.println(ctx.getInitParameter("Nocturno")) ;
		String horarioNocturno = ctx.getInitParameter("Nocturno");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}



}
