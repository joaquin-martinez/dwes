package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class s4
 */
@WebServlet("/s4")
public class s4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logS4 = Logger.getLogger("s4");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public s4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			PrintWriter out = response.getWriter() ;
			String value ;
			value =request.getParameter("cod");
			if(value != null)
				out.println("<p>Se ha pasado el parametro: " + value + "</p>" );
			else
				out.println("<p>No se ha pasado el parametro \"cod\". </p>" );
			
			Enumeration<String> params = request.getParameterNames() ;
			while(params.hasMoreElements()) {
				String parametro = params.nextElement() ;
				out.println("<p>" + parametro + " - " + request.getParameter(parametro) + "</p>");
			}
	
			
			Map<String,String[]> params2 = request.getParameterMap() ;
			params2.forEach((x,y)->logS4.info(x + " - " + Arrays.stream(y).collect(Collectors.joining(",")) ));
//				out.println("<p>" + params2.nextElement() + " - " + request.getParameter(params2.nextElement()) + "</p>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
