package webservices;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/s1")
public class ws1 {

	@GET
	public String get() {
		return "Peticion Get";
	}
	
	@POST
	public String post1() {
		return "Peticion Post";
	}
}
