package webservices;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

@Path("/s3")
public class ws3 {
	private static final Logger logTag = Logger.getLogger("servicios");
		/* procesamiento de formularios con submit */
	@GET
	@Path("/edad")  //  ws/s3/edad?nombre=joaquin&edad=50
	public String userGet(@QueryParam("nombre") String nom , @QueryParam("edad") int anios  ) {
		
		String consecuencia1 = " ES mayor de edad" ;
		String consecuencia2 = " Es menor de edad" ;
		String conse = (18 <= anios)?consecuencia1:consecuencia2;
		return "get: Nombre : " + nom + " , edad : " + anios + conse;
		
	}

	@GET /* queremos la suma de valores */
	@Path("/suma") /* ws/s3/suma?x=5&y=3&.... */
	public String operaDosOperrandos(@Context  UriInfo ui ) {
		int x=0 , y=0 ;
		//obtener los arametros de la request.
		MultivaluedMap<String , String> params = ui.getQueryParameters();
		//numero de parametros: params.values().size;
		//if(params.values().size == 2 && params.get("x") != null && && params.get("y") != null)
		if (params.values().size() == 2 && params.containsKey("x") && 
				params.containsKey("y")) {
			try {
				x= Integer.valueOf(params.get("x").get(0));
				y= Integer.valueOf(params.get("y").get(0));
				
			}catch (NumberFormatException e) {
				
			}
			
			
		}
		
		return x + " + " + y + " = " + (x+y) ;
		
	}
	
	// hacerlo para distinto numero de sumandos y sin importar las keys.
	
	@GET
	@Path("/sumamultiple")
	public String multipleSuma(@Context  UriInfo ui) {
		String salida;
		List<String> valores;
		int suma ;
		MultivaluedMap<String , String> params = ui.getQueryParameters();
		suma = params.values().stream().map(x-> x.get(0)).collect(Collectors.summingInt(x-> Integer.parseInt(x)));
		salida = params.values().stream().map(x-> x.get(0)).collect(Collectors.joining(" + "));
		logTag.info( salida + " = " + suma);
		salida = salida + " = " + suma;
		return salida;
	}
	
	
	@GET
	@Path("/sumar")
	public String sumaMultiple(@Context  UriInfo ui) {
		String salida ="";
		String valor;
		Collection<List<String>> listaValores;
		Iterator<List<String>> iterListaValores;
		int suma = 0 ;
		MultivaluedMap<String , String> params = ui.getQueryParameters();
		listaValores = params.values();
		iterListaValores = listaValores.iterator();
		while(iterListaValores.hasNext()) {
			valor = iterListaValores.next().get(0);
			suma = suma + Integer.parseInt(valor);
			salida = salida + " + " + valor; 
		}
		salida = salida + " = " + suma;
		salida = salida.substring(2,salida.length());
		logTag.info( "RESULTADO = " + salida );
		return salida;
	}
	
	
	}
