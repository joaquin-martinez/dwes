package webservices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/s2")
public class ws2 {

	@POST
//	@GET  // URI: <server>/ws/s2/joa -> hola joa
	@Path("{nombre}")
	public String getSaludo( @PathParam("nombre") String name) {
		return "hola " + name ;
	}

/*
	@GET  // URI: <server>/ws/s2/432 -> el numero es 432
	@Path("{numero: \\d+}")
	public String getNumero( @PathParam("numero") String n) {
		return "El numero es " + n ;
	}
	
*/

	@POST
//	@GET  // URI: <server>/ws/s2/ac-432 -> el codigo es ac-432
	@Path("{codigo:[a-z][a-z][-]\\d{3}}")
	public String getCodigo( @PathParam("codigo") String n) {
		return "El codigo es " + n ;
	}

	/* Ejercicio:
	calcular a partir de una peticion post y del paramentro de la URL UN
	NUMERO factorial de un numero mayor que uno
	*/
	

	@POST
//	@GET
	@Path("{num:[1-9]}")
	public String getFactorial( @PathParam("num") String n) {
		int numero ;
		int result = 1 ;
		numero = Integer.parseInt(n);
		for (int i=1 ; i<=numero ; i++) {
			result = result * i;
		}
		System.out.println(Integer.toString(result));
		return "El factorial de " + n + " es: " +  Integer.toString(result);
	}
	
	/* Ejercicio:
	 * Calcular a partir de una peticion post y del parametro de la URL
	 * el n�mero de digitos pasados (pueden haber letras).
	 */
	
	@POST
//	@GET
	@Path("{entrada}")
	public String getNumeroDigitos( @PathParam("entrada") String cadena) {
		int numeroDigitos = 0;
		int caracteres ;
		
		caracteres = cadena.length();
		for(int i=0 ; i< caracteres ; i++) {
			int j=0;
			j = cadena.codePointAt(i);
			if (48<=j && j<= 57) {
				numeroDigitos ++ ; 
			}

		}
		return "El numero de digitos es: " + Integer.toString(numeroDigitos);
	
	}
	
	/* Ejercicio:
	 * Calcular a partir de una peticion get y de los parametros
	 * anio/mes/dia la fecha correspondiente: Por ejemplo si pasa 
	 * <URL principal>/2020/02/10 debera devolver: la fecha es: 10/02/2020
	 * con el formato DD/MM/YYYY.
	 */
	
//	@POST
	@GET
	@Path("{fecha}")
	public String getFecha( @PathParam("fecha") String n) {
		
		SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("ES"));
				   Date fechaDate ;
				   String fecha = "error";
					try {
						fechaDate = formateador.parse(n);
						   fecha = formateador.format(fechaDate);
						   System.out.println(fecha);

						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

		return "La fecha es: " + fecha;
	}
	
}
