package utilidades;

import java.util.LinkedList;

/**
 * Clase heredada de LinkedList de String que Implementa un método addRespuesta 
 * sincronizado para funcionar como lista compartida por varios hilos.
 * @author joaquin martinez
 */
public class ListaRespuestas extends LinkedList<String> {
    
    /**
     * Añade a la llista de forma sincrona.
     * @param respuesta
     * @return 
     */
    public synchronized boolean addRespuesta( String respuesta ){
        return this.add( respuesta);
    }
    
}
