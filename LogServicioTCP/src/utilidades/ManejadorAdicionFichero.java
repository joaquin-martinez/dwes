package utilidades;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase manejadora de un fichero en el que se va a ir añadiendo una lista de logs 
 * y que si no existe el fichero, lo crea.
 * @author Joaquín-Martínez
 */
public class ManejadorAdicionFichero {

    File fichero;
    FileWriter flujoEscritura;
    String nombreFichero;
    BufferedWriter bufferFlujoEscritura;

    public ManejadorAdicionFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
        
        fichero = new File(nombreFichero);

        if (!fichero.exists()) {
            try {
                fichero.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger("logser").log(Level.SEVERE, "Error al crear el fichero. ", ex);
            }
        }
        
    }

    public synchronized void  escribir(LinkedList<String> respuestas) throws IOException {

        if(fichero.canWrite()){
            
        flujoEscritura = new FileWriter(fichero, true);
        bufferFlujoEscritura = new BufferedWriter(flujoEscritura);
        
        respuestas.forEach(x->{
            try {
                bufferFlujoEscritura.write(x);
                bufferFlujoEscritura.flush();
            } catch (IOException ex) {
                Logger.getLogger("logser").log(Level.SEVERE, "Error al escribir en el fichero. ", ex);
            }
        });

        bufferFlujoEscritura.close();        
        flujoEscritura.close();
            
        }

    }

}
