package client;


import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase cliente que conecta con la clase servidora para mandarle su IP.
 * @author joaquin-martinez
 */
public class LogServicioTCPCliente {
    
 
    
    private static final String SERVER = "localhost";
    private static final int PUERTO = 8000;
    private static final Logger log = Logger.getLogger("logcli");
    
    /**
     * Método main ejecutor del cliente.
     * @param args 
     */
    public static void main(String[] args) {
        
        Socket clientSocket = null ;
        InetSocketAddress addr ;
        DataOutputStream flujoSalida = null ; 
        
//        log.setLevel(Level.OFF);
        log.setLevel(Level.ALL);
        
        try {

            clientSocket = new Socket();
            addr = new InetSocketAddress(SERVER, PUERTO);
            clientSocket.connect(addr);
            log.log(Level.INFO , "conectado");
            flujoSalida = new DataOutputStream(
            clientSocket.getOutputStream());

            String mensaje = "Hola soy el CLIENTE con dirección " + InetAddress.getLocalHost().getHostAddress();
            Logger.getLogger("logcli").log(Level.INFO , mensaje );
            flujoSalida.writeUTF(mensaje);


        } catch (IOException e) {
               Logger.getLogger("logcli").log(Level.SEVERE, "Error en la creacion del flujo. " , e);
        } finally {
            
            try {
                // Cerramos flujo si ha sido inicializado.
                if(flujoSalida != null)
                flujoSalida.close();
            } catch (IOException ex) {
                Logger.getLogger("logcli").log(Level.SEVERE, "error en el cierre de flujo. " , ex);
            }

            try {

                if(clientSocket != null)
                clientSocket.close();
                Logger.getLogger("logcli").log(Level.INFO , "Se cierra el Socket. ");
            } catch (IOException ex) {
                Logger.getLogger("logcli").log(Level.SEVERE, "error en el cierre de socket. ", ex);
            }
        }
    }   
    
}
