package server;

import utilidades.ListaRespuestas;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clasegenerada como hilo por el servidor para el tratamiento de la informacion 
 * de la IP del cliente, que junto a la hora del servidor, se registra en un
 * fichero log.
 * @author joaquin-martinez
 */
public class HiloLog extends Thread {
    
    Socket conexionCliente ;
    String apunte;
    LocalDate fecha ;
    LocalTime hora ;
    DataInputStream flujoEntrada;
 //   ManejadorAdicionFichero escribeLog ;
    ListaRespuestas respuestas ;

    HiloLog(Socket conexionCliente , ListaRespuestas respuestas ) {

      this.conexionCliente = conexionCliente ;
      this.respuestas = respuestas ;
      
    }
   
    /**
     * Implementación de la interfaz runable de los hilos.
     * Implementa lo descrito en la explicación de la clase.
     */
    public void run(){
        
       fecha = LocalDate.now();
       hora = LocalTime.now() ;
       apunte = Integer.toString(fecha.getDayOfMonth()) +
               "/" + Integer.toString(fecha.getMonthValue()) +
               "/" + Integer.toString(fecha.getYear()) + 
               ";" + Integer.toString(hora.getHour()) + 
               ":" + Integer.toString(hora.getMinute()) + 
               ":" + Integer.toString(hora.getSecond()) + 
               ";" ;
       Logger.getLogger("logser").log(Level.INFO , "Tomada fecha y hora del servidor: " + apunte);
                           
        try {
            flujoEntrada = new DataInputStream(
            conexionCliente.getInputStream());
            apunte = apunte + flujoEntrada.readUTF() + ",";
            System.out.println("MENSAJE A GRABAR: " + apunte);
            
        } catch (IOException ex) {
            Logger.getLogger("logser").log(Level.SEVERE, null, ex);
        } finally {
           respuestas.addRespuesta(apunte) ; 
           Logger.getLogger("logser").log(Level.INFO , "Se añade a la lista de mensajes: " + apunte );
           try {
               flujoEntrada.close();
               conexionCliente.close();
           } catch (IOException ex) {
               Logger.getLogger("logser").log(Level.SEVERE, null, ex);
           }
            
        }

    }
  
    
}
