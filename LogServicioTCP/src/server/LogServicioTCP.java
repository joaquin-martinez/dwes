
package server;

import utilidades.ListaRespuestas;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilidades.ManejadorAdicionFichero;

/**
 * Aplicativo encargado del proceso servidor para el log.
 * Pone el servidor a la escucha, acepta un número limitado de conexiones, 
 * crea el hilo que continua el proceso, crea la lista compartida para almacenar 
 * los resultados y al final los graba en un fichero log.
 * @author joaquin-martinez
 */
public class LogServicioTCP {
    
    private static final int PUERTO = 8000;
    private static final int NCONEXIONES = 6;
    private static final String nombreFichero = "conexioneslog.log" ;
    private static final Logger log = Logger.getLogger("logser");
    
    /**
     * Método main encargado de implementar la funcionalidad ya explicada de la clase.
     * @param arg 
     */
    public static void main(String[] arg){
        
    ServerSocket escucha ;
    Socket conexionCliente ;
    HiloLog hiloLog = null;
    ManejadorAdicionFichero escribeLog ;
    ListaRespuestas respuestas = new ListaRespuestas() ;
    
//    log.setLevel(Level.OFF );
    log.setLevel(Level.ALL );
       
        escribeLog = new ManejadorAdicionFichero(nombreFichero) ;
        
        try {
            escucha = new ServerSocket( PUERTO );
            log.log(Level.INFO, "Servidor a la escucha." );
            System.out.println("Servidor a la escucha ....");

            for(int i=0 ; i<NCONEXIONES ; i++){
                 
                    conexionCliente = escucha.accept();
                    hiloLog = new HiloLog( conexionCliente , respuestas );
                    log.log(Level.INFO, "Nuevo hilo." );
                    hiloLog.start();
                    
            }
            
            while(hiloLog.isAlive()){
                
            }
            escribeLog.escribir(respuestas);
            
            
            
        } catch (IOException ex) {
            Logger.getLogger("logser").log(Level.SEVERE, "Error en el servidor de conexiones.", ex);
        }
        
        
    }
    
}
