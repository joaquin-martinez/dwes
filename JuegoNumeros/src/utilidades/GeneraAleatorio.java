package utilidades;

import java.util.Random;

public class GeneraAleatorio {
	
	public static int aleatorio(int min, int max) {
		Random r = new Random();
		return min + r.nextInt(max - min + 1); // Entre min y max -> [min,max];
	}


}
