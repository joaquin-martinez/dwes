package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilidades.GeneraAleatorio;

/**
 * Servlet implementation class AdivinaNumeros
 */
@WebServlet({ "/adivinanumeros", "/juegonumeros" })
public class AdivinaNumeros extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdivinaNumeros() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	Integer numeroAleatorio;
	String numeroAleatorioString;
	String numeroUsuario;
	
		HttpSession sesion = request.getSession();
		numeroAleatorio = GeneraAleatorio.aleatorio(1, 99999);
		numeroAleatorioString = Integer.toString(numeroAleatorio);
		
		numeroUsuario = request.getParameter("numero");
		numeroUsuario = numeroUsuario.trim();
		numeroUsuario.matches("[0-9]{5}")
		
	
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
