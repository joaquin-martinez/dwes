package dao;

import java.util.List;

import model.Alojamiento;
import model.Tipo;

public interface DAO {
	public boolean alta(Alojamiento alojamiento);
	public List<Alojamiento> listarAlojamientos();
	public List<Tipo> listarTipos();
	public Alojamiento obtenerPorId(int id);
	public boolean modificar(Alojamiento articulo);
	public boolean baja(int id);
}
