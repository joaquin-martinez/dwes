package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import model.Alojamiento;
import model.Tipo;
import utilidades.Conexion;

public class DAOImpl implements DAO {
	private static final Logger logTag = Logger.getLogger(DAOImpl.class.getName());
	private Conexion conexion;
	
	public DAOImpl(String driver, String url, String bd, String username, String password) {
		this.conexion = new Conexion(driver, url, bd, username, password);
	}

	public DAOImpl(Properties props) {
		this.conexion = new Conexion(props);
	}

	@Override
	public boolean alta(Alojamiento alojamiento) {
		Connection c;
		boolean valueReturn = false;
		String sql = "INSERT INTO alojamiento ( nombre, poblacion, provincia, capacidad, tipo,"
				+ " ubicacion, alquilado) VALUES ( ?, ?, ?, ?, ?, ?, ?)";
		

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
//			statement.setInt(1, alojamiento.getReferencia());
			statement.setString(1, alojamiento.getNombre());
			statement.setString(2, alojamiento.getPoblacion());
			statement.setString(3, alojamiento.getProvincia());
			statement.setInt(4, alojamiento.getCapacidad());
			statement.setInt(5, alojamiento.getTipo());
			statement.setString(6, alojamiento.getUbicacion());
			statement.setBoolean(7, alojamiento.isAlquilado());


			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al insertar: " + e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public List<Alojamiento> listarAlojamientos() {
		Connection c;
		List<Alojamiento> listaAlojamientos = new ArrayList<Alojamiento>();
		String sql = "SELECT * FROM alojamiento";

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Consultar campos
				int referencia = resulSet.getInt("referencia");
				String nombre = resulSet.getString("nombre");
				String poblacion = resulSet.getString("poblacion");
				String provincia = resulSet.getString("provincia");
				int capacidad = resulSet.getInt("capacidad");
				int tipo = resulSet.getInt("tipo");
				String ubicacion = resulSet.getString("ubicacion");
				boolean alquilado = resulSet.getBoolean("alquilado");
				

				// Crear objeto Articulo
				Alojamiento alojamiento = new Alojamiento(referencia, nombre, poblacion, provincia, capacidad, 
						tipo, ubicacion , alquilado);

				// Insertar objeto en colecci�n
				listaAlojamientos.add(alojamiento);
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaAlojamientos;
	}

	@Override
	public Alojamiento obtenerPorId(int id) {
		Connection c;
		Alojamiento alojamiento = null;
		String sql = "SELECT * FROM alojamiento WHERE referencia= ? ORDER BY referencia";
		boolean encontrado = false;

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, id);

			ResultSet res = statement.executeQuery();
			if (res.next() && !encontrado) {
				if (res.getInt("referencia") == id) {
					alojamiento = new Alojamiento(res.getInt("referencia"), res.getString("nombre"), res.getString("poblacion"),
							res.getString("provincia"), res.getInt("capacidad"), res.getInt("tipo"), res.getString("ubicacion")
							,res.getBoolean("alquilado"));
					encontrado = true;
				}
			}

			// Cierre de conexiones
			res.close();
			this.conexion.desconectar();

		} catch (SQLException e) {
			logTag.severe("Error en la obtenci�n por ID: " + e.getMessage());
		}

		return alojamiento;
	}

	@Override
	public boolean modificar(Alojamiento alojamiento) {
		Connection c;
		boolean valueReturn = false;
		String sql = "UPDATE alojamiento SET nombre=?,poblacion=?,provincia=?, capacidad=? , tipo=?, ubicacion=? , alquilado=? WHERE referencia=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setString(1, alojamiento.getNombre());
			statement.setString(2, alojamiento.getPoblacion());
			statement.setString(3, alojamiento.getProvincia());
			statement.setInt(4, alojamiento.getCapacidad());
			statement.setInt(5, alojamiento.getTipo());
			statement.setString(6, alojamiento.getUbicacion());
			statement.setBoolean(7, alojamiento.isAlquilado());
			statement.setInt(8, alojamiento.getReferencia());

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al actualizar: "+ e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public boolean baja(int id) {
		Connection c;
		boolean valueReturn = false;
		String sql = "DELETE FROM alojamiento WHERE referencia=?";

		try {
			c = this.conexion.conectar();
			PreparedStatement statement = c.prepareStatement(sql);
			statement.setInt(1, id);

			// Valor de retorno
			valueReturn = statement.executeUpdate() > 0;

			// Cierre de conexiones
			statement.close();
			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al eliminar: " + e.getMessage());
		}

		return valueReturn;
	}

	@Override
	public List<Tipo> listarTipos() {

		Connection c;
		List<Tipo> listaTipos = new ArrayList<Tipo>();
		String sql = "SELECT * FROM tipos";

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Consultar campos
				int codigo = resulSet.getInt("codigo");
				String descripcion = resulSet.getString("descripcion");
		

				// Crear objeto Articulo
				Tipo tipo = new Tipo(codigo, descripcion);

				// Insertar objeto en colecci�n
				listaTipos.add(tipo);
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaTipos;

	}
}
