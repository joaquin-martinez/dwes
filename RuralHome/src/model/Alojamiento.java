package model;

public class Alojamiento {
	private int referencia;
	private String nombre;
	private String poblacion;
	private String provincia;
	private int capacidad;
	private int tipo;
	private String ubicacion;
	private boolean alquilado;

	public Alojamiento( int referencia, String nombre, String poblacion, String provincia, int capacidad, 
						int tipo, String ubicacion , boolean alquilado) {
		this.referencia = referencia;
		this.nombre = nombre;
		this.poblacion = poblacion;
		this.provincia = provincia;
		this.capacidad = capacidad;
		this.tipo = tipo;
		this.ubicacion = ubicacion;
		this.alquilado = alquilado;
		
	}

	public int getReferencia() {
		return referencia;
	}

	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public boolean isAlquilado() {
		return alquilado;
	}

	public void setAlquilado(boolean alquilado) {
		this.alquilado = alquilado;
	}
	
}
