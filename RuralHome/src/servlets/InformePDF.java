package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dao.DAO;
import model.Alojamiento;

/**
 * Servlet implementation class InformePDF
 */
@WebServlet("/informepdf")
public class InformePDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InformePDF() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sesion = request.getSession();
	DAO dao = (DAO) sesion.getAttribute("dao");
	List<Alojamiento> listaAlojamientos;
	
	listaAlojamientos = dao.listarAlojamientos();
	try {
		createPDF(response , listaAlojamientos);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * Creates a PDF document.
	 * 
	 * @param response
	 *            Main Response
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void createPDF(HttpServletResponse response , List<Alojamiento> listaAlojamientos) throws DocumentException, IOException {
		response.setContentType("application/pdf");

		Document document = new Document();
		PdfWriter.getInstance(document, response.getOutputStream());
		document.open();

		// Texto cabecera
		Paragraph title = new Paragraph("Informe de alojamientos alquilados");
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);

		// Generaci�n de tabla
		PdfPTable table = new PdfPTable(7); // N�mero de columnas principales
		table.addCell("Id");
		table.addCell("Nombre");
		table.addCell("Poblacion");
		table.addCell("Provincia");
		table.addCell("Capacidad");
		table.addCell("Tipo");
		table.addCell("ubicacion");



		for (Alojamiento a : listaAlojamientos) {
			PdfPCell celdaId = new PdfPCell(new Phrase(Integer.toString(a.getReferencia())));
			celdaId.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaId);

			PdfPCell celdaNombre = new PdfPCell(new Phrase(a.getNombre()));
			celdaNombre.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaNombre);

			PdfPCell celdaPoblacion = new PdfPCell(new Phrase(a.getPoblacion()));
			celdaPoblacion.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaPoblacion);
			
			PdfPCell celdaProvincia = new PdfPCell(new Phrase(a.getProvincia()));
			celdaProvincia.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaProvincia);

			PdfPCell celdaCapacidad = new PdfPCell(new Phrase(Integer.toString(a.getCapacidad())));
			celdaCapacidad.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaCapacidad);

			PdfPCell celdaTipo = new PdfPCell(new Phrase(Integer.toString(a.getTipo())));
			celdaTipo.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaTipo);

			PdfPCell celdaUbicacion = new PdfPCell(new Phrase(a.getUbicacion()));
			celdaUbicacion.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(celdaUbicacion);
		}
		document.add(table);

		// Cierre del documento
		document.close();
	}
	
}
