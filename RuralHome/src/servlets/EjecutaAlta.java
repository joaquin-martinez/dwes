package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import model.Alojamiento;

/**
 * Servlet implementation class EjecutaAlta
 */
@WebServlet("/ejecutaalta")
public class EjecutaAlta extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EjecutaAlta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	HttpSession sesion= request.getSession();
	if(sesion.isNew()) {
		response.encodeRedirectURL("index.html");
	}else {
		
		String nombre , poblacion , provincia , capacidad , ubicacion , tipo , alquiler;
		int capacidadNumero;
		int tipoNumero;
		boolean alquilerBoolean;
		Alojamiento alojamiento;
		DAO dao;
		String errorVacio = "Debe rellenar: ";
		boolean camposVacios = false;

		
		PrintWriter out = response.getWriter();
		dao = (DAO) sesion.getAttribute("dao");
		nombre = request.getParameter("nombre");
		nombre =nombre.trim();
		if(nombre.isEmpty()) {
			camposVacios = true;
			errorVacio += " nombre ";
		}

		poblacion = request.getParameter("poblacion");
		poblacion =poblacion.trim();
		if(poblacion.isEmpty()) {
			camposVacios = true;
			errorVacio += " poblacion ";
		}

		provincia = request.getParameter("provincia");
		provincia =provincia.trim();
		if(provincia.isEmpty()) {
			camposVacios = true;
			errorVacio += " provincia ";
		}

		capacidad = request.getParameter("capacidad");
		capacidad =capacidad.trim();
		if(capacidad.isEmpty() || !capacidad.matches("[0-9]{1,4}")) {
			camposVacios = true;
			errorVacio += " capacidad ";
		}

		tipo = request.getParameter("tipo");
		tipo =tipo.trim();
		if(tipo.isEmpty() || !tipo.matches("[0-9]{1,3}")) {
			camposVacios = true;
			errorVacio += " tipo ";
		}
		
		
		ubicacion = request.getParameter("ubicacion");
		ubicacion =ubicacion.trim();
		if(ubicacion.equals("undefined")) {
			camposVacios = true;
			errorVacio += " ubicacion ";
		}

		alquiler = request.getParameter("alquilado");
		if(alquiler.equals("undefined")) {
			camposVacios = true;
			errorVacio += " alquilado ";
		}
	
		if(!camposVacios) {

		capacidadNumero = Integer.parseInt(capacidad);
		tipoNumero = Integer.parseInt(tipo);
		alquilerBoolean = new Boolean(alquiler);
		
		alojamiento = new Alojamiento(0 , nombre , poblacion , provincia , capacidadNumero , tipoNumero , ubicacion , alquilerBoolean);
		
	
		
		System.out.println(nombre + " - " + poblacion + " - " + provincia + " - " + capacidad + " - " + tipo +
				" - " + ubicacion + " - " + alquiler);
		
		if(dao.alta(alojamiento)) {
			out.println("Ha sido dado de alta: " + nombre );
			
		}else {
			out.println("Error en el alta: " + nombre );
		}
	} else {
		out.println(errorVacio);
	}
		
	}
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
