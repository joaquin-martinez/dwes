package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import model.Alojamiento;

/**
 * Servlet implementation class EjecutaMod
 */
@WebServlet("/ejecutamod")
public class EjecutaMod extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EjecutaMod() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sesion= request.getSession();
		if(sesion.isNew()) {
			response.encodeRedirectURL("index.html");
		}else {
			

		String nombre , poblacion , provincia , capacidad , ubicacion , tipo , alquiler , referencia;
		int capacidadNumero;
		int tipoNumero;
		int referenciaNumero;
		boolean alquilerBoolean;
		Alojamiento alojamiento;
		DAO dao;
		String errorVacio = "Debe rellenar: ";
		boolean camposVacios = false;
//		Map<String , String[]> parametros;
//		int numeroCamposVacios;
		
		
		
		PrintWriter out = response.getWriter();
		
		
		dao = (DAO) sesion.getAttribute("dao");
		
//		parametros = request.getParameterMap();
//		 numeroCamposVacios = (int) parametros.entrySet().stream().filter((x)->x.getValue().length == 0).count();
		
//		 if(numeroCamposVacios >0) {
//			 out.println(errorVacio);
//		 }else {
		
		referencia = request.getParameter("referencia");
		referencia =referencia.trim();
		if(referencia.isEmpty()) {
			camposVacios = true;
			errorVacio += " referencia ";
		}
		nombre = request.getParameter("nombre");
		nombre =nombre.trim();
		if(nombre.isEmpty()) {
			camposVacios = true;
			errorVacio += " nombre ";
		}

		poblacion = request.getParameter("poblacion");
		poblacion =poblacion.trim();
		if(poblacion.isEmpty()) {
			camposVacios = true;
			errorVacio += " poblacion ";
		}

		provincia = request.getParameter("provincia");
		provincia =provincia.trim();
		if(provincia.isEmpty()) {
			camposVacios = true;
			errorVacio += " provincia ";
		}

		capacidad = request.getParameter("capacidad");
		capacidad =capacidad.trim();
		if(capacidad.isEmpty() || !capacidad.matches("[0-9]{1,4}")) {
			camposVacios = true;
			errorVacio += " capacidad ";
		}
		
		tipo = request.getParameter("tipo");
		tipo =tipo.trim();
		if(tipo.isEmpty() || !tipo.matches("[0-9]{1,3}")) {
			camposVacios = true;
			errorVacio += " tipo ";
		}
		
		ubicacion = request.getParameter("ubicacion");
		ubicacion =ubicacion.trim();
		if(ubicacion.isEmpty()) {
			camposVacios = true;
			errorVacio += " ubicacion ";
		}

		alquiler = request.getParameter("alquilado");
		if(alquiler.isEmpty()) {
			camposVacios = true;
			errorVacio += " alquilado ";
		}
		
		
		if(!camposVacios) {
		
		capacidadNumero = Integer.parseInt(capacidad);
		tipoNumero = Integer.parseInt(tipo);
		alquilerBoolean = new Boolean(alquiler);
		referenciaNumero =  Integer.parseInt(referencia);
		
		alojamiento = new Alojamiento(referenciaNumero , nombre , poblacion , provincia , capacidadNumero , tipoNumero , ubicacion , alquilerBoolean);
		
		
		if(dao.modificar(alojamiento)) {
			out.println("Ha sido modificado: " + nombre );
			
		}else {
			out.println("Error en la modificacion: " + nombre );
		}

		}else {
			out.println(errorVacio);
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
