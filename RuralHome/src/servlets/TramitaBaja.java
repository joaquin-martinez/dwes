package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import utilidades.Utilities;

/**
 * Servlet implementation class TramitaBaja
 */
@WebServlet("/tramitabaja")
public class TramitaBaja extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TramitaBaja() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String registro;
	boolean borrado;
	HttpSession sesion = request.getSession();
		PrintWriter out = response.getWriter() ;
		response.setContentType(Utilities.CONTENT_TYPE);
		DAO dao = (DAO) sesion.getAttribute("dao");
		System.out.println("Entra en baja.");
	registro = request.getParameter("registro");
	registro = registro.trim();
	if(!registro.isEmpty()) {
	borrado = dao.baja(Integer.parseInt(registro));
	System.out.println(registro);
	if(borrado) {
		
		out.println("registro con id " + registro + " ha sido borrado");
//		out.println("<script> getResponse(\"alojamientos\" , \"ruralHome\")</script>");
	}
	
	} else {
		out.println("Debe seleccionar algun registro a borrar.");
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
