package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import dao.DAOImpl;
import model.Alojamiento;

import utilidades.Utilities;

/**
 * Servlet implementation class RuralHome
 */
@WebServlet("/ruralhome")
public class RuralHome extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger logTag = Logger.getLogger("dwebs");

	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RuralHome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		HttpSession sesion = request.getSession();
		DAO dao = getDAO(request.getSession()); // Obtener DAO de la sesi�n

//		DAO dao = (DAO) sesion.getAttribute("dao");
		List<Alojamiento> listaAlojamientos;
		PrintWriter out = response.getWriter();
		response.setContentType(utilidades.Utilities.CONTENT_TYPE);
	
		listaAlojamientos = dao.listarAlojamientos();
	
//		out.println(Utilities.headWithTitle("RuralHome"));
		out.println();
		
		out.println("<table border='1'>");

		out.println("<tr>");
		out.println("<td>ID</td>");
		out.println("<td>NOMBRE</td>");
		out.println("<td>POBLACION</td>");
		out.println("<td>PROVINCIA</td>");
		out.println("<td>CAPACIDAD</td>");
		out.println("<td>TIPO</td>");
		out.println("<td>UBICACION</td>");
		out.println("<td>ALQUILADO</td>");
		out.println("<td>SELECCION</td>");
		out.println("</tr>");



		for (Alojamiento a : listaAlojamientos) {
			out.println("<tr>");
			out.println("<td>" + a.getReferencia() + "</td>");
			out.println("<td>" + a.getNombre() + "</td>");
			out.println("<td>" + a.getPoblacion() + "</td>");
			out.println("<td>" + a.getProvincia() + "</td>");
			out.println("<td>" + a.getCapacidad() + "</td>");
			out.println("<td>" + a.getTipo() + "</td>");
			out.println("<td>" + a.getUbicacion() + "</td>");
			out.println("<td>" + a.isAlquilado() + "</td>");
			out.println("<td>" + "<input type=\"radio\" name=\"seleccion\" id='" + a.getReferencia() + "' value='" + a.getReferencia()
					+ "' title='seleccionar'>" + "</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		// LISTADO

//		out.println("</body>");

		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}



	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada y par�metros
	 * inicializados en el descriptor de despliegue
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO
	 */
	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de par�metros del descriptor web.xml
			String driver = getServletContext().getInitParameter("driver");
			String url = getServletContext().getInitParameter("url");
			String bd = getServletContext().getInitParameter("bd");
			String username = getServletContext().getInitParameter("username");
			String password = getServletContext().getInitParameter("password");

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", new DAOImpl(driver, url, bd, username, password));

			// 3. Nuevo DAO generado para la sesi�n
			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}
}
