package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import dao.DAOImpl;
import model.Tipo;
import utilidades.Utilities;

/**
 * Servlet implementation class DameTipos
 */
@WebServlet("/dametipos")
public class DameTipos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger logTag = Logger.getLogger("dwebs");

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DameTipos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		boolean valueReturn = false; // Valor de retorno en cada modificación del modelo
		List<Tipo> listaTipos;
		HttpSession sesion = request.getSession();
		DAO dao = (DAO) sesion.getAttribute("dao");		
		listaTipos = dao.listarTipos();
		sesion.setAttribute("tipo", listaTipos );
	PrintWriter out = response.getWriter();
	response.setContentType(Utilities.CONTENT_TYPE);
	for(Tipo tipo : listaTipos) {
		out.println("<option value=\"" + tipo.getCodigo() + "\" >" + tipo.getDescripcion() + "</option>");
	}
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	
}
