package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DAO;
import model.Alojamiento;
import model.Tipo;
import utilidades.Utilities;

/**
 * Servlet implementation class TramitaCambios
 */
@WebServlet("/tramitacambios")
public class TramitaCambios extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TramitaCambios() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		Alojamiento alojamiento;
		List<Tipo> listaTipos;
		String registro ;
		int posicionTipo = -2;
		boolean cambiado;
		String ubica1 = "" ;
		String ubica2 = "";
		String alquilasi = "";
		String alquilano = "";
		HttpSession sesion = request.getSession();
			PrintWriter out = response.getWriter() ;
//			response.setContentType(Utilities.CONTENT_TYPE);
			DAO dao = (DAO) sesion.getAttribute("dao");
			listaTipos = (List<Tipo>) sesion.getAttribute("tipo");
			System.out.println("Entra en modificaciones.");
		registro = request.getParameter("registro");
		System.out.println("Entra en modificaciones." + registro);
		registro = registro.trim();
		if(!registro.isEmpty()) {

		alojamiento = dao.obtenerPorId(Integer.parseInt(registro));
//		posicionTipo = listaTipos.indexOf(listaTipos.stream().filter( x-> x.getCodigo()==alojamiento.getTipo()  ).findFirst()  );
		try {
			Tipo tipoa = listaTipos.stream().filter(x->x.getCodigo()==(alojamiento.getTipo())).findFirst().orElseThrow(()-> new Exception("No se encuentra el tipo"));
			System.out.println("el tipo encontrado es." + tipoa.getCodigo() + " contenido: " + tipoa.getDescripcion());
		
			posicionTipo = listaTipos.indexOf(tipoa);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("la posicion del tipo es." + posicionTipo);
		System.out.println("Entra en modificaciones." + alojamiento.getNombre());
		out.println("<fieldset>");
		out.println("<legend>Modificaciones</legend>");
		out.println("<form action=\"javascript:grabaCambios();\" name='mod' id='mod' >");
		out.println("<label for=\"nombrem\" >Nombre: </label>");
		out.println("<input type=\"text\" name=\"nombrem\" id=\"nombrem\" value='" + alojamiento.getNombre() + "'><br>");
		out.println("<label for=\"poblacionm\" >Poblacion: </label>");
		out.println("<input type=\"text\" name=\"poblacionm\" id=\"poblacionm\" value='" + alojamiento.getPoblacion() + "'><br>");
		out.println("<label for=\"provinciam\" >Provincia: </label>");
		out.println("<input type=\"text\" name=\"provinciam\" id=\"provinciam\" value='" + alojamiento.getProvincia() + "'><br>");
		out.println("<label for=\"tipom\" >Tipo: </label>");
		out.println("<select name=\"tipom\" id=\"tipom\">");
		for(int i =0; i<posicionTipo ; i++) {
			out.println( "<option value='" + listaTipos.get(i).getCodigo() + "' >" + listaTipos.get(i).getDescripcion()
					+ "</option>");
		}
		out.println( "<option value='" + listaTipos.get(posicionTipo).getCodigo() + "' selected='selected' >" + listaTipos.get(posicionTipo).getDescripcion()
				+ "</option>");
		for(int i =posicionTipo +1; i<listaTipos.size() ; i++) {
			out.println( "<option value='" + listaTipos.get(i).getCodigo() + "' >" + listaTipos.get(i).getDescripcion()
					+ "</option>");
		}
		
		out.println("</select><br>");
		out.println("<label for=\"capacidadm\" >Capacidad: </label>");
		out.println("<input type=\"text\" name=\"capacidadm\" id=\"capacidadm\" value='" + alojamiento.getCapacidad() + "'><br>");
		out.println("<label for=\"ubicacionm\" >Ubicacion: </label>");
		if(alojamiento.getUbicacion().equals("aislado")) {
			ubica1 = " ";
			ubica2 = " checked = 'checked' ";
		}else {
			ubica1 = " checked = 'checked' ";
			ubica2 = " ";
			
		}
		
		out.println("<input type=\"radio\" name=\"ubicacionm\" id=\"ubicacion1m\" " + ubica1 + " value=\"poblacion\" >En poblacion");
		out.println("<input type=\"radio\" name=\"ubicacionm\" id=\"ubicacion2m\" " + ubica2 + "  value=\"aislado\" >Aislado");
		out.println("<br>");
		out.println("<label for=\"alquiladom\" >Alquilado: </label>");
		
		if(alojamiento.isAlquilado()) {
			alquilasi = " checked = 'checked' ";
			alquilano = " ";
		}else {
			alquilasi = " ";
			alquilano = " checked = 'checked' ";
			
		}

		
		out.println("<input type=\"radio\" name=\"alquiladom\" id=\"alquilado1m\" " + alquilasi +  " value=\"true\"  >Si");
		out.println("<input type=\"radio\" name=\"alquiladom\" id=\"alquilado2m\" " + alquilano + "  value=\"false\" >No");
		out.println("<br>");
		out.println("<input type=\"hidden\" name=\"referencia\" id=\"referencia\" value=\"" + alojamiento.getReferencia() + "\">");

		out.println("<input type=\"submit\" name=\"actualiza\" id=\"actualiza\" value=\"actualiza\">");
		out.println("</form>");
		out.println("</fieldset>");
		} else {
			out.println("Debe seleccionar algun registro a modificar.");
			out.println("<script> eval( alert('adios'));</script>");

			out.println("<script src=\"javascript:activaAlta()\"></script> ");

		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
