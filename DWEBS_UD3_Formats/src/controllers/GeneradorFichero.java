package controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Servlet implementation class GeneradorFichero
 */
@WebServlet("/generadorfichero")
public class GeneradorFichero extends HttpServlet {
	private final String fileNameExcel = "prueba.xls" ;
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeneradorFichero() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String peticionFormato;
		peticionFormato = request.getParameter("format");
		if(peticionFormato==null)
		
		
		try {
			createPDF(response);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		/**
		 * Creates a PDF document.
		 * 
		 * @param response
		 *            Main Response
		 * @throws DocumentException
		 * @throws IOException
		 */
		public void createPDF(HttpServletResponse response) throws DocumentException, IOException {
			response.setContentType("application/pdf");

			Document document = new Document();
			PdfWriter.getInstance(document, response.getOutputStream());
			document.open();

			// Texto cabecera
			Paragraph title = new Paragraph("STOCK");
			title.setAlignment(Element.ALIGN_CENTER);
			document.add(title);

			// Generaci�n de tabla
			PdfPTable table = new PdfPTable(3); // N�mero de columnas principales
			table.addCell("Nombre");
			table.addCell("Precio");
			table.addCell("Cantidad");

			for (int i = 0; i < 9; i++) {
				PdfPCell celda = new PdfPCell(new Phrase("*"));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(celda);
			}
			document.add(table);

			// Cierre del documento
			document.close();
		}
	
	
		public void creaXLS(HttpServletResponse response) throws IOException {
		

		response.setContentType("application/xls");
		
		// modoficar la linea de cabecera de la respuesta para descarga del fichero.
		response.setHeader("Content-Disposition", "filename=" + fileNameExcel);
		
		PrintWriter out = response.getWriter();
		out.println("nombre\tapellido\tedad\t");
		out.println("joaquin\tmartinez\t50\t");

		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
