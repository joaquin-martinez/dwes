package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import beans.Contactos;
import dao.DAO;
import dao.DAOImpl;
import modelo.GestorFicheros;
import beans.Departamento;
import beans.ListaDepartamentos;
import serializadores.ProcesadorJAXB;
import utils.Utilities;
import beans.Contacto;

/**
 * Servlet implementation class Agenda
 */
@MultipartConfig
@WebServlet("/agenda")
public class Agenda extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logTag = Logger.getLogger("agenda");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Agenda() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @throws IOException
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		GestorFicheros gestorFotos = new GestorFicheros(request);
		DAO dao;
		PrintWriter out;
		String error = "\"aqui van los errores\"";

		try {
			out = response.getWriter();
		} catch (IOException e1) {
			logTag.log(Level.WARNING, "Ha ocurrido un error al obtener el PrintWriter desde el request.");
			e1.printStackTrace();
			throw new IOException("Ha ocurrido un error al obtener el PrintWriter desde el request.");
		}
		HttpSession sesion = request.getSession();
		ProcesadorJAXB<Contacto> procesador = new ProcesadorJAXB<>();
		String accion;

		dao = getDAO(sesion);

		if (sesion.isNew()) {

			// Iniciamos la carpeta img del servidor con los ficheros subidos por el usuario
			// del programa.
			gestorFotos.copiaFotosToImg();

		}

		// Seg�n la accion que nos pidan. ejecutamos un m�todo.
		accion = request.getParameter("accion");
		System.out.println(" LA accion es :   " + accion);
		if (accion != null) {
			switch (accion) {
			case "alta":
				try {
					altaContacto(request, dao, gestorFotos);
				} catch (IOException e) {
					logTag.log(Level.WARNING,
							"Ha ocurrido un error en los procesos de manejo de las fotos durante el alta");
					error = "Ha ocurrido un error en los procesos de manejo de las fotos durante el alta";
					e.printStackTrace();
				} finally {
					getContactos(request, response, dao, out, procesador, error);
				}
				logTag.info("daar de alta");
				break;
			case "modificar":
				try {
					modificacionContacto(request, dao, gestorFotos);
				} catch (IOException e) {
					logTag.log(Level.WARNING,
							"Ha ocurrido un error en los procesos de manejo de las fotos durante la modificacion");
					error = "Ha ocurrido un error en los procesos de manejo de las fotos durante la modificacion";
					e.printStackTrace();
				} finally {
					getContactos(request, response, dao, out, procesador, error);
				}
				logTag.info("modificara");
				break;
			case "baja":
				try {
					bajaContacto(request, dao, gestorFotos);
				} catch (IOException e) {
					logTag.log(Level.WARNING,
							"Ha ocurrido un error en los procesos de manejo de ficheros durante baja");
					error = "Ha ocurrido un error en los procesos de manejo de ficheros durante baja";
					e.printStackTrace();
				} finally {
					getContactos(request, response, dao, out, procesador, error);
				}
				break;
			case "listdep":
				getDepartamentos(response, dao);
				break;
			case "vcard":
				getVcard(request, response, dao, out);
				break;
			case "listemp":
				getContactos(request, response, dao, out, procesador, error);
				break;
			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO Devuelve el dao que nos da acceso a la BD.
	 */

	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de la conexion hibernate.
			DAOImpl dao = new DAOImpl();

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", dao);

			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}

	private boolean altaContacto(HttpServletRequest request, DAO dao, GestorFicheros gestorFotos)
			throws IOException, ServletException {

		Contacto contacto;
		String nombreFoto;
		String id;
		Part fichero;
		String extension = "jpg";
		logTag.log(Level.INFO, "iniciamos el alta ");

		fichero = request.getPart("foto");
		if ((fichero.getContentType()).split("/")[0].equals("image")) {
			logTag.log(Level.INFO, "Viene con imagen ");
			logTag.log(Level.INFO,fichero.getSubmittedFileName());
			extension =fichero.getSubmittedFileName().split("\\.")[1];
			id = (String) request.getSession().getAttribute("idSig");

			nombreFoto = "foto" + id + "." + extension;

			gestorFotos.guardaFichero(fichero, nombreFoto);

		} else {
			logTag.log(Level.INFO, "Viene sin con imagen ");
			nombreFoto = "foto.jpg";

		}

		contacto = new Contacto(request.getParameter("nombre"), request.getParameter("apellido1"),
				request.getParameter("apellido2"), "img/" + nombreFoto, request.getParameter("email"),
				request.getParameter("telMovil"), request.getParameter("telFijo"),
				request.getParameter("departamento"));
		logTag.log(Level.INFO, "creado contacto para alta ");

		return dao.insertar(contacto);
	}

	private boolean modificacionContacto(HttpServletRequest request, DAO dao, GestorFicheros gestorFotos)
			throws IOException, ServletException {

		Contacto contacto;
		String nombreFoto;
		String id;
		String extension;
		Part fichero;
		logTag.log(Level.INFO, "iniciamos modificaciones ");

		fichero = request.getPart("foto");
		if ((fichero.getContentType()).split("/")[0].equals("image")) {
			extension =fichero.getSubmittedFileName().split("\\.")[1];
			id = request.getParameter("id");
			System.out.println("El id recuperado para el nombre foto es: " + id);
			nombreFoto = "foto" + id + "." + extension;

			gestorFotos.guardaFichero(fichero, nombreFoto);

		} else {
			nombreFoto = request.getParameter("dirFoto").split("/")[1];

		}

		id = request.getParameter("id");
		contacto = new Contacto(request.getParameter("nombre"), request.getParameter("apellido1"),
				request.getParameter("apellido2"), "img/" + nombreFoto, request.getParameter("email"),
				request.getParameter("telMovil"), request.getParameter("telFijo"),
				request.getParameter("departamento"));
		contacto.setId(Integer.parseInt(request.getParameter("id")));
		logTag.log(Level.INFO, "id recibido po parametro " + request.getParameter("id"));
		return dao.actualizar(contacto);
	}

	private boolean bajaContacto(HttpServletRequest request, DAO dao, GestorFicheros gestorFotos) throws IOException {

		String extension = "jpg" ;
		logTag.log(Level.INFO, "entrando en baja ");
		extension = request.getParameter("dirfoto").split("\\.")[1];
		logTag.log(Level.INFO, request.getParameter("dirfoto").split("\\.")[1]);
		int id = Integer.parseInt(request.getParameter("id"));
		logTag.log(Level.INFO, "baja del id: " + id);
		gestorFotos.borraFotoImg("foto" + id + "." + extension);
		return dao.eliminar(id);

	}

	private void getVcard(HttpServletRequest requesst, HttpServletResponse response, DAO dao, PrintWriter out) {
		final String fileNameVCARD = "agenda.vcf";

		// generamos los contactos para el fichero vcard.
		List<Contacto> listaContactos;
		response.setContentType("text/vcard");
		response.setHeader("Content-Disposition", "filename=" + fileNameVCARD);

		listaContactos = dao.listarContactos();
		logTag.info("Preparando Vcard : ");
		// Cumplimentamos el formato vcard con los datos de los empleados.
		for (Contacto cont : listaContactos) {
			out.println("BEGIN:VCARD");
			out.println("VERSION:3.0");
			out.println("FN:" + cont.getNombre() + " " + cont.getApellido1() + " " + cont.getApellido2());
			out.println("N:" + cont.getApellido1() + ";" + cont.getNombre() + ";" + cont.getApellido2() + ";;");
			out.println("EMAIL;TYPE=HOME,INTERNET,pref:" + cont.getEmail());
			out.println("TEL;TYPE=VOICE,HOME;VALUE=text:" + cont.getTelFijo());
			out.println("TEL;TYPE=VOICE,CELL;VALUE=text:" + cont.getTelMovil());
			out.println("TEL;TYPE=VOICE,WORK;VALUE=text:" + cont.getDepartamento());
			out.println("END:VCARD");
			out.println();
		}

	}

	private void getDepartamentos(HttpServletResponse response, DAO dao) throws IOException {

		List<Departamento> listaDepartamentos;
		ListaDepartamentos departamentos = new ListaDepartamentos();
		String salida2;
		PrintWriter out2 = response.getWriter();
		ProcesadorJAXB<Contacto> procesador = new ProcesadorJAXB<>();

		listaDepartamentos = dao.listarDepartamentos();
		(departamentos.getDepartamentos()).addAll(listaDepartamentos);
		salida2 = procesador.serializarJSON(departamentos);
		System.out.println("Salida de departamentos: " + salida2);
		response.setContentType(Utilities.CONTENT_TYPE_JSON);
		out2.println(salida2);

	}

	private void getContactos(HttpServletRequest request, HttpServletResponse response, DAO dao, PrintWriter out,
			ProcesadorJAXB<Contacto> procesador, String error) {

		List<Contacto> listaContactos = null;
		Contactos contactos = new Contactos();
		String salida;
		String sal;
		String idSig;

		listaContactos = dao.listarContactos();
		idSig = Integer.toString(listaContactos.get(listaContactos.size() - 1).getId() + 1);
		logTag.info(idSig);
		request.getSession().setAttribute("idSig", idSig);

		(contactos.getContacto()).addAll(listaContactos);
		salida = procesador.serializarJSON(contactos);
		response.setContentType(Utilities.CONTENT_TYPE_JSON);

		sal = salida.substring(0, salida.length() - 1);
		sal = sal + ",\"error\":" + error + "}";
		logTag.info("Sal es : " + sal);
		logTag.info("Salida es : " + salida);

		out.println(sal);

	}

}
