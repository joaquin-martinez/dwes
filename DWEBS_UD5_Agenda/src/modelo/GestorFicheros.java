package modelo;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 * Gestiona el trabajo que tenemos que realizar con los ficheros subidos.
 * 
 * @author Joaquin Martinez Perez
 *
 */
public class GestorFicheros {
	private static final Logger logTag = Logger.getLogger("agenda");

	private File carpetaImg;
	private File carpetaFotos;

	public GestorFicheros(HttpServletRequest request) {
		carpetaImg = new File(request.getServletContext().getRealPath("") + File.separator + "img");
		carpetaFotos = new File(
				".." + File.separator + "standalone" + File.separator + "tmp" + File.separator + "fotos");
		if (!carpetaFotos.exists())
			carpetaFotos.mkdirs();

	}

	/**
	 * Actualiza la carpeta img con las fotos subidas por el usuario del programa.
	 * 
	 * @param carpetaFotos
	 *            Ubicacion de la carpeta fotos. Si no existe, la crea.
	 * @param carpetaImg
	 *            Ubicacion de la carpeta img.
	 * @throws IOException
	 */
	public void copiaFotosToImg() throws IOException {

		logTag.info("LA direccion de la carpeta de fotos en el servidor es: " + carpetaFotos.getAbsolutePath());
		logTag.info("LA direccion de la carpeta de imagenes en el servidor es: " + carpetaImg.getAbsolutePath());

		for (String nombreFichero : carpetaFotos.list()) {
			logTag.info("Se copiara el fichero" + nombreFichero);
			copiaFotoToImg(nombreFichero);
		}
	}

	public void copiaFotoToImg(String nombreFoto) throws IOException {

		Path copiado;
		Path origen = Paths.get(carpetaFotos.getAbsolutePath(), nombreFoto);
		logTag.info("ruta origen: " + origen.toString());
		Path destino = Paths.get(carpetaImg.getAbsolutePath(), nombreFoto);
		logTag.info("ruta destino: " + destino.toString());
		copiado = Files.copy(origen, destino, StandardCopyOption.REPLACE_EXISTING);
		logTag.info("copiado a: " + copiado.toString());

	}

	public void borraFotoImg(String nombreFoto) throws IOException {

		Path fotoCarpeta = Paths.get(carpetaFotos.getAbsolutePath(), nombreFoto);
		Path fotoImg = Paths.get(carpetaImg.getAbsolutePath(), nombreFoto);

		if (fotoCarpeta.toFile().exists())
			Files.delete(fotoCarpeta);
		if (fotoImg.toFile().exists())
			Files.delete(fotoImg);

	}

	public void guardaFichero(Part fichero, String nombreFoto) throws IOException {

		File archivoPer = new File(carpetaFotos.getPath() + File.separator + nombreFoto);
		System.out.println("tama�o:  " + archivoPer.length());
		System.out.println("localizacion:  " + archivoPer.getAbsolutePath());

		if (archivoPer.exists()) {
			logTag.info("Tenia otra foto y se borrara");
			// archivoPer.renameTo(dest);
			archivoPer.delete();
		}

		fichero.write(archivoPer.getAbsolutePath());
		System.out.println("tama�o:  " + archivoPer.length());
		System.out.println("localizacion:  " + archivoPer.getAbsolutePath());

		copiaFotoToImg(nombreFoto);

	}
}
