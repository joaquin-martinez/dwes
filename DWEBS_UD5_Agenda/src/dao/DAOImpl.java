package dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import beans.Contacto;
import beans.Departamento;
import utils.ConexionHibernate;

public class DAOImpl implements DAO {

	private static final Logger logTag = Logger.getLogger("dwebs");

	ConexionHibernate conexion = new ConexionHibernate();
	Session c = null;

	@Override
	public boolean insertar(Contacto contacto) {

		boolean insertado = true;
		try {

			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			c.save(contacto);
			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
			e.printStackTrace();
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback(); // Transacción: Rollback
				insertado = false;
			}
		} finally {
			conexion.desconectar(); // Cierre de conexión
		}

		return insertado;
	}

	@Override
	public List<Contacto> listarContactos() {

		List<Contacto> listaContactos;
		c = conexion.conectar(); // Inicio de conexión
		listaContactos = c.createQuery("from Contacto").list();
		conexion.desconectar(); // Cierre de conexión

		return listaContactos;
	}

	@Override
	public List<Contacto> listarContacto(String departamento) {
		List<Contacto> listaContactos;
		Query consulta;
		c = conexion.conectar(); // Inicio de conexión
		consulta = c.createQuery("from Contacto  where departamento.departamento= :departamento order by id");
		consulta.setString("departamento", departamento);
		listaContactos = consulta.list();
		conexion.desconectar(); // Cierre de conexión
		return listaContactos;
	}

	@Override
	public List<Departamento> listarDepartamentos() {

		List<Departamento> listaCategorias;
		c = conexion.conectar(); // Inicio de conexión
		listaCategorias = c.createQuery("from Departamento").list();
		conexion.desconectar(); // Cierre de conexión

		return listaCategorias;

	}

	@Override
	public Contacto obtenerPorId(int id) {

		Contacto contacto;
		c = conexion.conectar(); // Inicio de conexión
		contacto = (Contacto) c.get(Contacto.class, id);
		conexion.desconectar(); // Cierre de conexión
		return contacto;
	}

	@Override
	public boolean actualizar(Contacto contacto) {

		boolean actualizado = true;
		try {
			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			c.update(contacto);
			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
			System.out.println("fallo en la actualizacion");
			e.printStackTrace();
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback();
				actualizado = false;
			}

		} finally {
			conexion.desconectar(); // Cierre de conexión
		}

		return actualizado;
	}

	@Override
	public boolean eliminar(int id) {

		boolean eliminado = true;

		try {
			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			Contacto a = (Contacto) c.get(Contacto.class, id);
			if (a != null) {
				c.delete(a);
			} else {
				eliminado = false;
			}

			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
//			logTag(Level.INFO , "fallo en la actualizacion");
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback();
				eliminado = false;
			}
		}

		return eliminado;
	}

}
