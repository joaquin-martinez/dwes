package dao;

import java.util.List;

import beans.Contacto;
import beans.Departamento;


public interface DAO {
	public boolean insertar(Contacto contacto);

	public List<Contacto> listarContactos();

	public List<Contacto> listarContacto(String departamento);

	public List<Departamento> listarDepartamentos();

	public Contacto obtenerPorId(int id);

	public boolean actualizar(Contacto contacto);

	public boolean eliminar(int id);
}
