package beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import beans.Contacto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contacto"
})
@XmlRootElement(name = "Contactos")
public class Contactos {

	protected List<Contacto> contacto;
	
	public List<Contacto> getContacto(){
        if (contacto == null) {
            contacto = new ArrayList<Contacto>();
        }
        return this.contacto;
	}
}
