package beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import beans.Contacto;


	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {
	    "departamento"
	})
	@XmlRootElement(name = "departamento")
	public class ListaDepartamentos {

		protected List<Departamento> departamento;
		
		public List<Departamento> getDepartamentos(){
	        if (departamento == null) {
	        	departamento = new ArrayList<Departamento>();
	        }
	        return this.departamento;
		}
}
