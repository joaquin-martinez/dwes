DROP DATABASE IF EXISTS agenda;
CREATE DATABASE agenda CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE agenda;

-- TABLAS
DROP TABLE IF EXISTS departamento;
CREATE TABLE departamento (
	departamento varchar(20) NOT NULL,
	PRIMARY KEY (departamento)
);


DROP TABLE IF EXISTS contacto;
CREATE TABLE contacto (
	id int(6) NOT NULL AUTO_INCREMENT,
	nombre varchar(50) NOT NULL,
	apellido1 varchar(50) NOT NULL,
	apellido2 varchar(50) NOT NULL,
	foto varchar(50) NOT NULL,
	email varchar(30) NOT NULL,
	tel_movil varchar(15) NOT NULL,
	tel_fijo varchar(15) NOT NULL,
	departamento varchar(20),
	PRIMARY KEY (id)
);

-- DATOS
INSERT INTO departamento VALUES ('709'),('109'),('309'),('009'),('509'),('909');

INSERT INTO contacto ( nombre, apellido1, apellido2, foto, email,  tel_fijo , tel_movil, departamento) VALUES
( 'JOSE', 'GARCIA', 'RUEDA', 'img/foto3.jpg', 'jose@empresa.es','954252523' , '655454543' , '709'),
( 'JUAN', 'MARTINEZ', 'ROBLES', 'img/foto4.jpg', 'juan@empresa.es','954252524' , '655454544' , '109'),
( 'MARIA', 'GOMEZ', 'SEGURA', 'img/foto5.jpg', 'maria@empresa.es','954252525' , '655454545' , '509'),
( 'ROSA', 'SEGURA', 'TORRES', 'img/foto6.jpg', 'rosa@empresa.es', '954252526' , '655454546' , '109'),
( 'JOAQUIN', 'VAZQUEZ', 'NAVARRO', 'img/foto7.jpg', 'joaquin@empresa.es', '954252527' , '655454547' , '709'),
( 'AMALIA', 'FERNANDEZ', 'CONTRERAS', 'img/foto8.jpg', 'amalia@empresa.es', '954252528' , '655454548' , '709');
