var emp;
var dep;
var empleado;
var idtel;
var id;

$(function() {
	// Acciones para la preparacion del menu de altas, ya preparado por defecto.
	$(altaac).hide();

	var formDep = new FormData();
	formDep.append("accion" , "listdep" );
	 // Preparacion de la accion de cargar los departamentos
	// Solicitud de los datos de los departamentos.
	peticiones("agenda", "html", "post", formDep , "selectDepartamentos");
	// Solicitud de los datos de los contactos.
	var formEmp = new FormData();
	formEmp.append("accion" , "listemp" );
	setTimeout(peticiones, 1000, "agenda", "html", "post", formEmp, "tablaEmpleados");
	
	// Prepara el escenario para hacer las modificaciones sobre el formulario.
	$(modificacion).on('click', function(event) {
		event.preventDefault();
		var idemp = $('input:radio[name=rad]:checked').val();
		if (!idemp) {
			console.log("debe seleccionar un empleado");
			alert("Debe seleccionar el registro a modificar.");
		} else {
			console.log(idemp);
			empleado = emp[idemp];
			console.log(empleado);
			nombre.value = empleado.nombre;
			$(apellido1).val(empleado.apellido1);
			$(apellido2).val(empleado.apellido2);
			$(email).val(empleado.email);
			$(telFijo).val(empleado.telFijo);
			$(telMovil).val(empleado.telMovil);
			$(departamento).val(empleado.departamento);
			$(dirFoto).val(empleado.foto);
			id = empleado.id;

			$(altaac).show();
			$(grabar).val("grabar modificaciones");
			$(accion).val("modificar")
			$(legend).html("Modificaciones");
			$(reseteo).hide();
		}
	});

	// Acciones para la preparacion del formulario de altas,
	$(altaac).on('click', function(event) {
		event.preventDefault();
		$(altaac).hide();
		$(grabar).val("grabar alta");
		$(accion).val("alta")
		$(legend).html("Altas");
		$(reseteo).show();
		$(reseteo).click();
	});

	$(baja).on(
			'click',
			function(event) {
				event.preventDefault();
				var idemp = $('input:radio[name=rad]:checked').val();
				if (!idemp) {
					console.log("debe seleccionar un empleado");
					alert("Debe seleccionar el registro a dar de baja.");
				} else {
					if (confirm("baja")) {
						empleado = emp[idemp];
						console.log(empleado);
						var formData = new FormData();
						formData.append("id", empleado.id);
						formData.append("dirfoto", empleado.foto);
						formData.append("accion", "baja");

						peticiones("agenda", "html", "post", formData,	"tablaEmpleados");
					}
					;
				}
			});

	/* Envio de formulario formDatos. */

	$("#formDatos").on('submit', function(event) {
		event.preventDefault();
		if(!$("#telFijo").validate() && $("#telMovil").validate()){
		var formul = document.forms.namedItem("formDatos")
		var formData = new FormData(formul);
		formData.append("id", id);
		peticiones("agenda", "html", "post", formData, "tablaEmpleados");

		$(altaac).hide();
		$(grabar).val("grabar alta");
		$(accion).val("alta")
		$(legend).html("Altas");
		$(reseteo).show();
		$(reseteo).click();
		}
	});

});

peticiones = function(servidor, tipo, metodo, parametros, funcion) {
	$.ajax({
		url : servidor,
		type : metodo,
		dataType : tipo,
		data : parametros,
		cache : false,
		contentType : false,
		processData : false,
		success : function(datos) {
			eval(funcion + "(datos)");
		}
	});
}

selectDepartamentos = function(data) {
	console.log(data);
	dep = (JSON.parse(data)).departamento.departamento;
	$.each(dep, function(index, value) {
		if (valor = value.departamento)
			$("#departamento").append(
					"<option value=" + valor + ">" + value.departamento
							+ "</option>");
	});
}

function tablaEmpleados(data) {

	emp = (JSON.parse(data)).Contactos.contacto;
	error= (JSON.parse(data)).error ;
	console.log(error);
	$(infoerror).text(error);
	var tabla = $("#tblEmpl");
	tabla.html("");

	$.each(	emp, function(index, value) {
		tabla.append("<tr>");
		tabla.append("<td><img src='" + value.foto + "' alt='" + index 
								+ "'class='btnEditar' title='editar'/>"	+ value.id + "</td>");
		tabla.append("<td>" + value.nombre + " " + value.apellido1										+ " " + value.apellido2 + "</td>");
		tabla.append("<td>" + value.email + "</td>");
		tabla.append("<td>" + value.telFijo + "</td>");
		tabla.append("<td>" + value.telMovil + "</td>");
		tabla.append("<td>" + value.departamento + "</td>");
		tabla.append("<td><input type ='radio' name='rad' value=" + index + " /></td>");
		tabla.append("</tr>");
		
		
					});

}
