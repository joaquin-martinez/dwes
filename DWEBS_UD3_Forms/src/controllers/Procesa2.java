package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilidades.CompruebaValores;
import utilidades.Utilities;

/**
 * Servlet implementation class Procesa2
 */
@WebServlet("/procesa2")
public class Procesa2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Procesa2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		String edad = request.getParameter("edad");
		String email = request.getParameter("email");
		String[] departamentos = request.getParameterValues("departamentos");
		String[] lenguajes = request.getParameterValues("lenguajes");
		String salida = "";
		String[] resultados;

		CompruebaValores compruebaValoresUsuario = new CompruebaValores(nombre, apellidos, edad, email);

		resultados = compruebaValoresUsuario.comprobarRegistro2(departamentos, lenguajes); // .comprobar();

		if (resultados[1].isEmpty())
			salida = resultados[0] + "<br> <a href=\"registro2.html\">Nuevo registro</a>";
		else {
			salida = resultados[1] + "<br><p>Registro fallido.</p> <a href=\"registro2.html\">Reintentar</a>";
		}

		PrintWriter out = response.getWriter();
		out.println(Utilities.headWithTitle("Formulario de entrada."));
		out.println("<body>\n\r" + salida + "\n\r</body>\n\r<html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
