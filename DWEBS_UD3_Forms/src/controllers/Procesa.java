package controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilidades.CompruebaValores;
import utilidades.Utilities;

/**
 * Servlet implementation class Procesa
 */
@WebServlet("/procesa")
public class Procesa extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Procesa() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String resultados[];
		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		String edad = request.getParameter("edad");
		String email = request.getParameter("email");
		String salida = "";
		CompruebaValores compruebaValoresUsuario = new CompruebaValores(nombre, apellidos, edad, email);

		resultados = compruebaValoresUsuario.comprobar();

		if (resultados[1].isEmpty())
			salida = resultados[0];
		else {
			salida = resultados[1] + "<br> <a href=\"registro.html\">volver</a>";
		}

		PrintWriter out = response.getWriter();
		out.println(Utilities.headWithTitle("Formulario de entrada."));
		out.println("<body>\n\r <p>" + salida + "</p>\n\r</body>\n\r<html>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private boolean comprobadorVacios(String valor) {

		boolean vacio;

		valor = valor.trim();
		if (vacio = (valor.isEmpty() || valor.equals("") || valor == null))
			;

		return vacio;
	}

}
