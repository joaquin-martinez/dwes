package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ProcesaBuscador
 */
@WebServlet("/buscador")
public class ProcesaBuscador extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProcesaBuscador() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String buscador = request.getParameter("buscador");
		String termino = request.getParameter("cadena");

		if (buscador != null && termino != null && !termino.isEmpty()) {

			switch (buscador) {
			case "google":
				response.sendRedirect("http://google.es/search?q=" + termino);
				break;

			case "googleI":
				response.sendRedirect("http://google.es/search?q=" + termino + "&tbm=isch&amp;sa=X");
				break;

			case "bing":
				response.sendRedirect("http://bing.es?q=" + termino);
				break;

			case "yahoo":
				response.sendRedirect("http://es.search.yahoo.com/search?q=" + termino);
				break;

			}

		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "busqueda no encontrada.");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
