package utilidades;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CompruebaValores {

	private String nombre;
	private String apellidos;
	private String edad;
	private String email;
	private String salidaErrores = "";
	private String salidaValores = "";
	private String resolucion[];

	public CompruebaValores(String nombre, String apellidos, String edad, String email) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.email = email;
		resolucion = new String[2];

	}

	public String[] comprobar() {

		if (comprobadorVacios(nombre))
			salidaErrores += "<p>Falta el nombre.</p>";
		else {
			salidaValores += "<li> nombre : " + nombre + "</li>";
		}

		if (comprobadorVacios(apellidos))
			salidaErrores += "<p>Faltan los apellidos.</p>";
		else {
			salidaValores += "<li> apellidos : " + apellidos + "</li>";
		}

		if (comprobadorVacios(edad))
			salidaErrores += "<p>Falta la edad.</p>";
		else {
			if (!edad.matches("[1-9][0-9]{0,2}"))
				salidaErrores += "<p>No es una edad valida.</p>";
			else {
				salidaValores += "<li> edad : " + edad + "</li>";
			}
		}

		if (comprobadorVacios(email))
			salidaErrores += "<p>Falta el email.</p>";
		else {
			if (!email.matches("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$"))
				salidaErrores += "<p>No es una direccion de correo valida</p>";
			else {
				salidaValores += "<li> email : " + email + "</li>";
			}

		}
		salidaValores = "<ul>" + salidaValores + "</ul>";
		resolucion[0] = salidaValores;
		resolucion[1] = salidaErrores;

		return resolucion;
	}

	public String[] comprobarRegistro2(String[] departamentos, String[] lenguajes) {

		comprobar();

		if (departamentos == null || departamentos.length == 0)
			salidaErrores += "<p>Debe seleccionar al menos un departamento.</p>";
		else {
			salidaValores += "<p>Departamentos: </p>" + "<ul><li>" + obtenerValores(departamentos) + "</li></ul>";
		}

		if (lenguajes == null || lenguajes.length == 0)
			salidaErrores += "<p>Debe seleccionar al menos un lenguaje.</p>";
		else {
			salidaValores += "<p>Lenguajes: </p>" + "<ul><li>" + obtenerValores(lenguajes) + "</li></ul>";

		}

		resolucion[0] = salidaValores;
		resolucion[1] = salidaErrores;

		return resolucion;
	}

	private boolean comprobadorVacios(String valor) {

		boolean vacio;

		valor = valor.trim();
		if (vacio = (valor.isEmpty() || valor.equals("") || valor == null))
			;

		return vacio;
	}

	private String obtenerValores(String[] valores) {
		String cadenaValores = "";

		cadenaValores = Arrays.stream(valores).collect(Collectors.joining("</li><li>"));
		System.out.println(cadenaValores);

		return cadenaValores;
	}

}
