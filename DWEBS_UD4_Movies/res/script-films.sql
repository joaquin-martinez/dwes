DROP DATABASE IF EXISTS films;
CREATE DATABASE films CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE films;

-- TABLAS
DROP TABLE IF EXISTS film;
CREATE TABLE film (
	id int(10) NOT NULL AUTO_INCREMENT,
	titulo varchar(150) NOT NULL,
	director varchar(50) NOT NULL,
	anio varchar(4) NOT NULL,
	resumen varchar(500) NOT NULL,
	minutos varchar(5) NOT NULL,
	thumbnail varchar(150) ,
	url varchar(150) NOT NULL,
	PRIMARY KEY (id)
);


-- DATOS
INSERT INTO film (titulo, director, anio, resumen, minutos, thumbnail, url ) VALUES
('Blade Runner 2049', 'Denis Villeneuve', '2017' ,
'Treinta a�os despu�s de los eventos del primer film, un nuevo blade runner, K (Ryan Gosling) descubre un secreto largamente oculto que podr�a acabar con el caos que impera en la sociedad. El descubrimiento de K le lleva a iniciar la b�squeda de Rick Deckard (Harrison Ford), un blade runner al que se le perdi� la pista hace 30 a�os. ',
'163', 'http://es.web.img3.acsta.net/pictures/17/08/25/11/58/463146.jpg', 'https://www.youtube.com/watch?v=S_JAMRKzEHs'),
('Charlot en el teatro', 'Charles Chaplin', '1915',
'Charlot acude al teatro, perfectamente vestido de etiqueta. Y all� se acumulan situaciones graciosas y molestas, algunas reconocibles para cualquiera... ',
'29', '', 'http://www.youtube.es'),
('Los Minions', 'Kyle Balda,  Pierre Coffin', '2015',
'La historia de Los Minions se remonta al principio de los tiempos. Empezaron siendo organismos amarillos unicelulares que evolucionaron a trav�s del tiempo, poni�ndose siempre al servicio de los amos m�s despreciables. Ante su incapacidad para mantener a esos amos � desde el T. Rex a Napole�n �, los Minions acaban encontr�ndose solos y caen en una profunda depresi�n. Sin embargo, uno de ellos, llamado Kevin, tiene un plan. Acompa�ado por el rebelde Stuart y el adorable Bob, emprende un emocionante viaje para conseguir una jefa a quien servir, la terrible Scarlet Overkill. Pasar�n de la helada Ant�rtida, a la ciudad de Nueva York en los a�os sesenta, para acabar en el Londres de la misma �poca, donde deber�n enfrentarse al mayor reto hasta la fecha: salvar a la raza Minion de la aniquilaci�n. ',
'91', 'https://pics.filmaffinity.com/minions-199956794-mmed.jpg', 'https://youtu.be/5Ds5_ecFmCw'),
('Cars', 'John Lasseter', '2006',
'El aspirante a campe�n de carreras Rayo McQueen parece que est� a punto de conseguir el �xito, la fama y todo lo que hab�a so�ado, hasta que por error toma un desv�o inesperado en la polvorienta y solitaria Ruta 66. Su actitud arrogante se desvanece cuando llega a una peque�a comunidad olvidada que le ense�a las cosas importantes de la vida que hab�a olvidado.',
'116', 'https://pics.filmaffinity.com/cars-746710621-mmed.jpg', 'https://youtu.be/1uq5eJHwio4'),
('Mary Poppins', 'Robert Stevenson', '1964',
'Reinado de Eduardo VII (1901-1910). La vida de una familia inglesa formada por un padre banquero, una madre sufragista y dos ni�os rebeldes -que pretenden llamar la atenci�n de sus padres haciendo la vida imposible a todas las ni�eras-, se ver� alterada con la llegada de Mary Poppins, una extravagante institutriz que baja de las nubes empleando su paraguas como paraca�das. Debut y Oscar para Julie Andrews en este cl�sico del cine familiar que en su d�a bati� r�cords de taquilla. Una ni�era m�gica y canciones pegadizas que har�n las delicias de los ni�os de la casa. ',
'140', 'https://pics.filmaffinity.com/mary_poppins-798780385-mmed.jpg', 'https://youtu.be/7ce5-7wh95Q'),
('El bueno, el feo y el malo', 'Sergio Leone', '1966',
'Durante la guerra civil norteamericana (1861-1865), tres cazadores de recompensas buscan un tesoro que ninguno de ellos puede encontrar sin la ayuda de los otros dos. As� que colaboran entre s� para conseguir el bot�n. ',
'161', 'https://pics.filmaffinity.com/il_buono_il_brutto_il_cattivo-351606800-mmed.jpg', 'https://youtu.be/z5tew9-G3zg'),
('El jinete p�lido', 'Clint Eastwood', '1985',
'Un grupo de colonos buscadores de oro se establece en un lugar de California, pero sufren el acoso de los hombres de Lahood, el propietario del resto de las explotaciones mineras. Pero un d�a al poblado llega un misterioso y fr�o predicador (Clint Eastwood) que se pone de parte de los colonos, y comienza a enfrentarse al temido cacique local.',
'113', 'https://pics.filmaffinity.com/pale_rider-746285316-mmed.jpg', 'https://youtu.be/ePlyWlhobDU'),
('El nombre de la rosa', 'Jean-Jacques Annaud', '1986',
'Siglo XIV. Fray Guillermo de Baskerville (Sean Connery), monje franciscano y antiguo inquisidor, y su inseparable disc�pulo, el novicio Adso de Melk (Christian Slater), visitan una abad�a benedictina, situada en el norte de Italia, para esclarecer la muerte del joven miniaturista Adelmo de Otranto. Durante su estancia, desaparecen misteriosamente otros monjes que despu�s aparecen muertos. ',
'131', 'https://pics.filmaffinity.com/der_name_der_rose_le_nom_de_la_rose-561341919-mmed.jpg', 'https://youtu.be/7-yYJgpQ-CE'),
('Conan, el barbaro', 'John Milius', '1982',
'Un ni�o que pertenece a una tribu primitiva graba en su memoria los rostros de los guerreros que han exterminado a su familia y a �l lo han vendido a unos mercaderes de esclavos. A�os despu�s, el joven se ha convertido en un forzudo y valiente guerrero.',
'129', 'https://pics.filmaffinity.com/conan_the_barbarian-679144812-mmed.jpg', 'https://www.youtube.com/watch?v=H7K6C8lIL7Q'),
('Con la muerte en los talones', 'Alfred Hitchcock', '1959',
'Debido a un malentendido, a Roger O. Thornhill, un ejecutivo del mundo de la publicidad, unos esp�as lo confunden con un agente del gobierno llamado George Kaplan. Secuestrado por tres individuos y llevado a una mansi�n en la que es interrogado, consigue huir antes de que lo maten. Cuando al d�a siguiente regresa a la casa acompa�ado de la polic�a, no hay rastro de las personas que hab�a descrito. ',
'136', 'https://i.pinimg.com/736x/18/ff/4e/18ff4e09c6d8534f42c646d67bbfa9b4--manuel-alfred-hitchcock.jpg', 'https://www.youtube.com/watch?v=lr7bH3ZnZ8M');


