package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import beans.Film;
import utilidades.Conexion;

/**
 * Clase que implementa el DAO para la obtenci�n de los objetos Film a partir de la base de datos, haciendo independiente la logica
 *  de la aplicaci�n de la fuente de datos.
 * @author joaquin
 *
 */
public class DAOImpl implements DAO {
	private static final Logger logTag = Logger.getLogger(DAOImpl.class.getName());
	private Conexion conexion;
/**
 * Constructor de la implementaci�n DAO que toma los par�metros de la base de datos mysql.
 * @param driver . Driver que utilizamos.
 * @param url . Direcci�n del servidor de la base de datos.
 * @param bd . Nombre de la base de datos a utilizar.
 * @param username . Usuario con atribuciones para operar con las tablas.
 * @param password . Clave de acceso del usuario anteriormente descrito.
 */
	public DAOImpl(String driver, String url, String bd, String username, String password) {
		this.conexion = new Conexion(driver, url, bd, username, password);
	}

	/**
	 * Constructor de la implementaci�n DAO que toma los par�metros de la base de datos mysql a partir de un
	 *  fichero de propiedades.
	 * @param props . Objeto Properties, manejador del fichero de propiedades.
	 */
	public DAOImpl(Properties props) {
		this.conexion = new Conexion(props);
	}

	/**
	 * M�todo que extare de la base de datos la lista de objetos peliculas.
	 */
	@Override
	public List<Film> listarFilms() {
		Connection c;
		List<Film> listaFilms = new ArrayList<Film>();
		String sql = "SELECT * FROM film order by anio DESC";

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Extraer campos
				String titulo = resulSet.getString("titulo");
				String director = resulSet.getString("director");
				String anio = resulSet.getString("anio");
				String resumen = resulSet.getString("resumen");
				String minutos = resulSet.getString("minutos");
				String thumbnail = resulSet.getString("thumbnail") ;
				String url = resulSet.getString("url");
				// Crear objeto Film
				Film film = new Film(titulo,director, anio, resumen, minutos
						, thumbnail, url);

				// Insertar objeto en colecci�n
				listaFilms.add(film);
				logTag.info("Extraido registro en filmoteca. " + film.getTitulo());
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaFilms;
	}

	
/**
 * M�todo que extare de la base de datos la lista de objetos peliculas del a�o pasado por parametro.
 */
	@Override
	public List<Film> listarFilmsAnio(String fecha) {
		Connection c;
		List<Film> listaFilms = new ArrayList<Film>();
		String sql = "SELECT * FROM film WHERE anio LIKE " + fecha;

		try {
			c = this.conexion.conectar();
			Statement statement = c.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				// Extraer campos
				String titulo = resulSet.getString("titulo");
				String director = resulSet.getString("director");
				String anio = resulSet.getString("anio");
				String resumen = resulSet.getString("resumen");
				String minutos = resulSet.getString("minutos");
				String thumbnail = resulSet.getString("thumbnail") ;
				String url = resulSet.getString("url");
				// Crear objeto Film
				Film film = new Film(titulo,director, anio, resumen, minutos
						, thumbnail, url);

				// Insertar objeto en colecci�n
				listaFilms.add(film);
				logTag.info("Extraido registro en filmoteca: " + film.getTitulo() + " del a�o " + film.getAnio());
			}

			this.conexion.desconectar();
		} catch (SQLException e) {
			logTag.severe("Error al consultar: " + e.getMessage());
		}

		return listaFilms;		
	}


}
