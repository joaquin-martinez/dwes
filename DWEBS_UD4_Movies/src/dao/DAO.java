package dao;

import java.util.List;

import beans.Film;

public interface DAO {

	public List<Film> listarFilms();

	public List<Film> listarFilmsAnio(String anio);

}
