package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import beans.Film;
import dao.DAO;
import dao.DAOImpl;
import utilidades.Utilities;

/**
 * Servlet implementation class Movies
 */
@WebServlet("/movies")
public class Movies extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logTag = Logger.getLogger("pelis");

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Movies() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Film> films;
		String gsonFilms;
		DAO dao;
		HttpSession sesion = request.getSession();
		Gson gson = new Gson();
		
		dao = getDAO(sesion);
		
		films = dao.listarFilms();
		gsonFilms = gson.toJson(films);
		
		logTag.info(gsonFilms);
		
		gsonFilms = "{\"films\":{\"film\":" + gsonFilms + "}}" ;
		
		response.setContentType(Utilities.CONTENT_TYPE_JSON);
		PrintWriter out = response.getWriter();
		
		out.println(gsonFilms);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada y par�metros
	 * inicializados en el descriptor de despliegue
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO
	 */
	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de par�metros del descriptor web.xml
			String driver = getServletContext().getInitParameter("driver");
			String url = getServletContext().getInitParameter("url");
			String bd = getServletContext().getInitParameter("bd");
			String username = getServletContext().getInitParameter("username");
			String password = getServletContext().getInitParameter("password");

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", new DAOImpl(driver, url, bd, username, password));

			// 3. Nuevo DAO generado para la sesi�n
			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}


}
