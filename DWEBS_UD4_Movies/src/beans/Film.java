package beans;

public class Film {
	private String titulo;
	private String director;
	private String anio;
	private String resumen;
	private String minutos;
	private String thumbnail ;
	private String url;
	
	public Film(String titulo, String director, String anio, String resumen, String minutos, String thumbnail,
			String url) {
		super();
		this.titulo = titulo;
		this.director = director;
		this.anio = anio;
		this.resumen = resumen;
		this.minutos = minutos;
		this.thumbnail = thumbnail;
		this.url = url;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getResumen() {
		return resumen;
	}

	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	public String getMinutos() {
		return minutos;
	}

	public void setMinutos(String minutos) {
		this.minutos = minutos;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	

}
