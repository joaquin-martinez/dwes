package xpath;

import java.io.File;

import javax.xml.xpath.XPathExpressionException;

import utils.ProcesadorXPath;

public class CochesXPath {
	public static void main(String args[]) throws Exception {
		File xml = new File("res" + File.separator + "coches.xml");

		if (xml.exists())
			try {
				// EJEMPLO 1
				System.out.println("*************************");
				String exp = "//coche[fabricante='Nissan']";
				System.out.println(exp);
				System.out.println(ProcesadorXPath.consulta(xml, exp));

				// EJEMPLO 2
				System.out.println("*************************");
				exp = "//coche[fabricante='Renault' and unidades>15]";
				System.out.println(exp);
				System.out.println(ProcesadorXPath.consulta(xml, exp));

				// EJEMPLO 3
				System.out.println("*************************");
				exp = "count(//coche)";
				System.out.println(exp);
				System.out.println(ProcesadorXPath.consulta(xml, exp));

			} catch (XPathExpressionException e) {
				System.err.println("-> Error en la expresión: " + e);
			} catch (Exception e) {
				System.err.println("-> Error: " + e);
			}
	}
}
