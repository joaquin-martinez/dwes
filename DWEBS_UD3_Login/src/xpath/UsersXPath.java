package xpath;

import java.io.File;

import javax.xml.xpath.XPathExpressionException;

import utils.ProcesadorXPath;

public class UsersXPath {
	public static void main(String args[]) throws Exception {
		File xml = new File("res" + File.separator + "users.xml");

		if (xml.exists())
			try {
				// EJEMPLO 1
				System.out.println("*************************");
				String exp = "//user[username='" + "admin" + "'][password='" + "1234" + "']/password/text()";
				System.out.println(exp);
				System.out.println(ProcesadorXPath.consulta(xml, exp).equals("1234"));
			} catch (XPathExpressionException e) {
				System.err.println("-> Error en la expresión: " + e);
			} catch (Exception e) {
				System.err.println("-> Error: " + e);
			}
	}
}
