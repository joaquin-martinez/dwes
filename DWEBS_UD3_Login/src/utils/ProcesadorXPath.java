package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;

public class ProcesadorXPath {
	/**
	 * Devuelve el resultado de una expresi�n XPath sobre un documento XML.
	 * 
	 * @param f
	 *            Documento XML de entrada
	 * @param txtexpresion
	 *            Expresi�n XPath
	 * @return Un objeto de tipo Node, Nodeset, String, Number o Boolean
	 * @throws XPathExpressionException
	 *             Si se produce un error en la expresi�n
	 * @throws FileNotFoundException
	 *             Si no se encuentra el fichero de entrada
	 */
	public static Object consulta(File f, String txtexpresion) throws XPathExpressionException, FileNotFoundException {
		InputSource entrada = new InputSource(new FileInputStream(f));

		// 1. Factor�a de creaci�n de objetos XPath
		XPathFactory factory = XPathFactory.newInstance();

		// 2. Usa la XPathFactory para crear un nuevo objeto XPath
		javax.xml.xpath.XPath xpath = factory.newXPath();

		// 3. Compila la expresi�n XPath para su correcci�n
		XPathExpression expresionXPath = xpath.compile(txtexpresion);

		// 4. Evalua la expresi�n XPath en el documento XML de entrada
		// 5. Devuelve el resultado de tipo XPathConstants
		return expresionXPath.evaluate(entrada);
	}
}
