package login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utils.Utilities;

/**
 * Servlet implementation class SessionContent
 */
@WebServlet("/content")
public class SessionContent extends HttpServlet {

	private final String loginUrl = "login";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();

		if (session != null) {
			String loggedIn = (String) session.getAttribute("loggedIn");

			if (loggedIn != null && loggedIn.equals("true")) {
				response.setContentType(Utilities.CONTENT_TYPE);
				PrintWriter out = response.getWriter();
				out.println(session.getId());
				out.println(Utilities.headWithTitle("Usuario autentificado"));
				out.println("<body>");
				out.println("<p>" + "Bienvenido" + "</p>");
				out.println("</body>");
				out.println("</html>");
			} else
				response.sendRedirect(loginUrl);
		} else
			response.sendRedirect(loginUrl);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
