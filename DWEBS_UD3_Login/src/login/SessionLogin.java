package login;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.xpath.XPathExpressionException;

import utils.ProcesadorXPath;
import utils.Utilities;

/**
 * Servlet implementation class SessionLoginServlet
 */
@WebServlet("/login")
public class SessionLogin extends HttpServlet {
	private final String contentUrl = "content";
	private final String filenameProperties = "users.properties";
	private final String filenameXML = "users.xml";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		sendLoginForm(response, false);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");

		if (loginFileXML(userName, password, filenameXML)) {
			// Env�a cookie al navegador
			HttpSession session = request.getSession();
			session.setAttribute("loggedIn", new String("true"));
			response.sendRedirect("content");
		} else {
			sendLoginForm(response, true);
		}
	}

	private void sendLoginForm(HttpServletResponse response, boolean errorMessage)
			throws ServletException, IOException {
		response.setContentType(Utilities.CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		out.println(Utilities.headWithTitle("Login"));
		out.println("<body>");
		out.println("<center>");

		if (errorMessage) {
			out.println("LOGIN FALLIDO. INT�NTALO DE NUEVO.<br/>");
		}

		out.println("<br/>");
		out.println("<br/><h2>Login</h2>");
		out.println("<br/>");
		out.println("<br/>Introduzca username y password");
		out.println("<br/>");
		out.println("<br/><form method=\"post\">");
		out.println("<table>");
		out.println("<tr>");
		out.println("<td>User Name:</td>");
		out.println("<td><input type=\"text\" name=\"username\"/></td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>Password:</td>");
		out.println("<td><input type=\"password\" name=\"password\"/></td>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td colspan=\"2\">");
		out.println("<button type=\"submit\">Login</button></td>");
		out.println("</tr>");
		out.println("</table>");
		out.println("</form>");
		out.println("</center>");
		out.println("</body>");
		out.println("</html>");
	}

	// Acceso login
	private boolean login(String userName, String password) {
		return userName.equals("1234") && password.equals("1234");
	}

	// Acceso login por fichero Properties
	private boolean loginFileProperties(String userName, String password, String filename) {
		boolean aux = false;
		Properties props = new Properties();

		try {
			// Carga en el contexto del hilo donde se encuentra el fichero
			props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Comprobaci�n del login del usuario si existe en las propiedades
		if (props != null && props.getProperty(userName) != null)
			aux = props.getProperty(userName).equals(password);

		return aux;
	}

	// Acceso login por XML y XPath
	private boolean loginFileXML(String userName, String password, String filename) {
		boolean aux = false;
		String exp;

		try {
			// Carga en el contexto del hilo donde se encuentra el fichero
			File xml = new File(Thread.currentThread().getContextClassLoader().getResource(filename).getFile());

			if (xml.exists()) {
				// 1. Generaci�n de la expresi�n XPath
				exp = "//user[username='" + userName + "'][password='" + password + "']/password/text()";
				System.out.println(exp);

				// 2. Consulta al Procesador XPath
				aux = ProcesadorXPath.consulta(xml, exp).equals(password);
			} else
				System.err.println("Fichero NO existe");
		} catch (XPathExpressionException e) {
			System.err.println("-> Error en la expresi�n: " + e);
		} catch (Exception e) {
			System.err.println("-> Error: " + e);
		}

		return aux;
	}
}
