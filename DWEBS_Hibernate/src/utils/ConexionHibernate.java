package utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ConexionHibernate {
	private Session hibernateConnection; // Representa una conexi�n a Hibernate

	/**
	 * Conecta a la capa de persistencia.
	 * 
	 * @return la sesi�n que relaciona la conexi�n a la capa de persistencia
	 * @throws HibernateException
	 *             devuelve una excepci�n si no se puede crear la conexi�n.
	 */
	public Session conectar() throws HibernateException {
		try {
			hibernateConnection = getConexion();
		} catch (Exception e) {
			throw new HibernateException("Error en la creaci�n de la conexi�n");
		}

		return this.hibernateConnection;
	}

	/**
	 * Desconecta de la capa de persistencia.
	 * 
	 * @return devuelve cierto si la conexi�n ha sido cerrada, falso en caso
	 *         contrario
	 * @throws HibernateException
	 *             devuelve una excepci�n si no puede cerrar la conexi�n.
	 */
	public boolean desconectar() {
		boolean closed = false;

		if (hibernateConnection != null && hibernateConnection.isOpen()) {
			hibernateConnection.close();
			closed = true;
		}

		return closed;
	}

	/**
	 * Devuelve la conexi�n a la capa de persistencia a partir de la sesi�n de la
	 * conexi�n
	 * 
	 * @return la conexi�n de la capa de persistencia de la sesi�n
	 * @throws HibernateException
	 *             devuelve una excepci�n si no se puede obtener la conexi�n de la
	 *             sesi�n
	 */
	public Session getConexion() throws HibernateException {
		if (hibernateConnection == null) {
			try {
				// Crea un constructor de factoria de sesiones para la configuraci�n:
				// hibernate.cfg.xml
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
						.configure("hibernate.cfg.xml").build();
				Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				SessionFactory factory = metadata.getSessionFactoryBuilder().build();
				hibernateConnection = factory.openSession();
			} catch (Throwable ex) {
				System.err.println("Hibernate SessionFactory fall� en su creaci�n: " + ex);
				throw new ExceptionInInitializerError(ex);
			}
		} else if (hibernateConnection != null && !hibernateConnection.isOpen()) {
			// Si previamente se ha creado y se ha cerrado, se genera una nueva sesi�n
			hibernateConnection = hibernateConnection.getSessionFactory().openSession();
		}

		return hibernateConnection;
	}
}
