package dao;

import java.util.List;

import entities.Articulo;
import entities.Categoria;

public interface DAO {
	public boolean insertar(Articulo articulo);

	public List<Articulo> listarArticulos();

	public List<Articulo> listarArticulos(String categoria);

	public List<Categoria> listarCategorias();

	public Articulo obtenerPorId(int id);

	public boolean actualizar(Articulo articulo);

	public boolean eliminar(int id);
}
