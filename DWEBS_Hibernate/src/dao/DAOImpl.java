package dao;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import entities.Articulo;
import entities.Categoria;
import utils.ConexionHibernate;

public class DAOImpl implements DAO {

	final Logger logTag = Logger.getLogger("dwebs");

	ConexionHibernate conexion = new ConexionHibernate();
	Session c = null;

	@Override
	public boolean insertar(Articulo articulo) {

		boolean insertado = true;
		try {

			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			c.save(articulo);
			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback(); // Transacción: Rollback
				insertado = false;
			}
		} finally {
			conexion.desconectar(); // Cierre de conexión
		}

		return insertado;
	}

	@Override
	public List<Articulo> listarArticulos() {

		List<Articulo> listaArticulos;
		c = conexion.conectar(); // Inicio de conexión
		listaArticulos = c.createQuery("from Articulo").list();
		conexion.desconectar(); // Cierre de conexión

		return listaArticulos;
	}

	@Override
	public List<Articulo> listarArticulos(String categoria) {
		List<Articulo> listaArticulos;
		Query consulta;
		c = conexion.conectar(); // Inicio de conexión
		consulta = c.createQuery("from Articulo  where categoria.categoria= :categoria order by id");
		consulta.setString("categoria", categoria);
		listaArticulos = consulta.list();
		conexion.desconectar(); // Cierre de conexión
		return listaArticulos;
	}

	@Override
	public List<Categoria> listarCategorias() {

		List<Categoria> listaCategorias;
		c = conexion.conectar(); // Inicio de conexión
		listaCategorias = c.createQuery("from Categoria").list();
		conexion.desconectar(); // Cierre de conexión

		return listaCategorias;

	}

	@Override
	public Articulo obtenerPorId(int id) {

		Articulo articulo;
		c = conexion.conectar(); // Inicio de conexión
		articulo = (Articulo) c.get(Articulo.class, id);
		conexion.desconectar(); // Cierre de conexión
		return articulo;
	}

	@Override
	public boolean actualizar(Articulo articulo) {

		boolean actualizado = true;
		try {
			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			c.update(articulo);
			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback();
				actualizado = false;
			}

		} finally {
			conexion.desconectar(); // Cierre de conexión
		}

		return actualizado;
	}

	@Override
	public boolean eliminar(int id) {

		boolean eliminado = true;

		try {
			c = conexion.conectar(); // Inicio de conexión
			c.getTransaction().begin(); // Transacción: Inicio
			Articulo a = (Articulo) c.get(Articulo.class, id);
			if (a != null) {
				c.delete(a);
			} else {
				eliminado = false;
			}

			c.getTransaction().commit(); // Transacción: Confirmación
		} catch (HibernateException e) {
			if (c.getTransaction() != null) { // Si se ha generado una transacción
				c.getTransaction().rollback();
				eliminado = false;
			}
		}

		return eliminado;
	}

}
