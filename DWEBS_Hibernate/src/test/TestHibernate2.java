package test;

import java.util.List;
import java.util.logging.Logger;

import dao.DAOImpl;
import entities.Articulo;

public class TestHibernate2 {

	public static void main(String[] args) {
		final Logger logTag = Logger.getLogger("dwebs");
		final int id = 11;
		DAOImpl dao = new DAOImpl();
		Articulo a = dao.obtenerPorId(id);

		if (a != null) {
			if (a.getPrecio() > 10)
				dao.eliminar(id); // Borrado
			else {
				a.setPrecio(11.50); // Actualizaci�n
				dao.actualizar(a);
			}
		} else {
			a = new Articulo(id, "AX-679", "LIBRO", "LIBRO DETALLE", 10, 9.20);
			dao.insertar(a); // Inserci�n
		}

		// Consulta de libros existentes
		List<Articulo> listaArticulos = dao.listarArticulos("CAJA");
		logTag.info("N� DE ART�CULOS CON CATEGORIA \"CAJA\": " + String.valueOf(listaArticulos.size()));
		for (Articulo articulo : listaArticulos) {
			logTag.info(articulo.getId() + " - " + articulo.getNombre() + " - " + articulo.getPrecio() + "� - "
					+ articulo.getCategoria().getCategoria());
		}
	}
}
