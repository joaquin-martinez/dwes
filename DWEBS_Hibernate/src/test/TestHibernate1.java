package test;

import java.util.List;
import java.util.logging.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import entities.Articulo;
import utils.ConexionHibernate;

public class TestHibernate1 {

	public static void main(String[] args) {
		final Logger logTag = Logger.getLogger("dwebs");
		final int id = 11;
		ConexionHibernate conexion = new ConexionHibernate();
		Session c = null;

		try {
			c = conexion.conectar(); // Inicio de conexi�n
			c.getTransaction().begin(); // Transacci�n: Inicio

			Articulo a = (Articulo) c.get(Articulo.class, id);

			if (a != null) {
				if (a.getPrecio() > 10)
					c.delete(a); // Eliminaci�n
				else {
					a.setPrecio(11.50);
					c.saveOrUpdate(a); // Actualizaci�n
					
					if (a.getCategoria() != null)
						c.saveOrUpdate(a.getCategoria());
				}
			} else {
				a = new Articulo(id, "AX-679", "LIBRO", "LIBRO DETALLE", 10, 9.20);
				c.save(a); // Inserci�n
				
				if (a.getCategoria() != null)
					c.saveOrUpdate(a.getCategoria());
			}

			// Consulta de art�culos de categor�a espec�fica existentes en la transacci�n
			// (�ojo! nombre de clase)
			List<Articulo> listaArticulos = c.createQuery("from Articulo where categoria.categoria='CAJA' order by id").list(); // Mirar: iterate()
			logTag.info("N� DE ART�CULOS: " + String.valueOf(listaArticulos.size()));
			for (Articulo articulo : listaArticulos)
				logTag.info(articulo.getId() + " - " + articulo.getNombre() + " - " + articulo.getPrecio() + "�");

			c.getTransaction().commit(); // Transacci�n: Confirmaci�n
		} catch (HibernateException e) {
			if (c.getTransaction() != null) // Si se ha generado una transacci�n
				c.getTransaction().rollback(); // Transacci�n: Rollback
		} finally {
			conexion.desconectar(); // Cierre de conexi�n
		}
	}
}