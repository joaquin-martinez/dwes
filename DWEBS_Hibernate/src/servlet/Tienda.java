package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import dao.DAO;
import dao.DAOImpl;
import entities.Articulo;
import entities.Categoria;
import utils.Utilities;

/**
 * Servlet implementation class Tienda
 */
// @WebServlet("/tienda")
public class Tienda extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logTag = Logger.getLogger("tienda");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Tienda() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Articulo> listaArticulos;
		List<Categoria> listaCategorias;
		boolean realizado = true;
		String accion;
		DAO dao;
		PrintWriter out = response.getWriter();
		HttpSession sesion = request.getSession();

		dao = getDAO(sesion);

		accion = request.getParameter("action");

		// Segun la accion, realizamos la tarea.
		if (accion != null) {
			switch (accion) {
			case "insertar":
				realizado = insertar(request, dao);
				break;
			case "actualizar":
				realizado = actualizar(request, dao);
				break;
			case "eliminar":
				realizado = eliminar(request, dao);

				break;
			}

		}

		listaArticulos = dao.listarArticulos();
		logTag.info("Obtenidos articulos de la bd en el servlet. ");
		listaCategorias = dao.listarCategorias();
		logTag.info("Obtenidas categorias de la bd en el servlet. ");

		JSONArray jsonArticulos = new JSONArray(listaArticulos);
		JSONArray jsonCategorias = new JSONArray(listaCategorias);
		logTag.info("Obtenidos los JSONArray. ");

		JSONObject jsonRespuesta = new JSONObject();
		jsonRespuesta.put("success", realizado);
		jsonRespuesta.put("categorias", jsonCategorias);
		jsonRespuesta.put("articulo", jsonArticulos);
		logTag.info("Obtenido el JSONObject respuesta. ");

		response.setContentType(Utilities.CONTENT_TYPE_JSON);

		jsonRespuesta.write(out);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * Obtiene un objeto DAO a partir de la sesi�n iniciada
	 * 
	 * @param sesion
	 *            Sesi�n iniciada del usuario
	 * @return Objeto DAO Devuelve el dao que nos da acceso a la BD.
	 */
	private DAO getDAO(HttpSession sesion) {
		if (sesion.isNew()) {
			// 1. Obtenci�n de la conexion hibernate.
			DAOImpl dao = new DAOImpl();

			// 2. Generar atributo en la sesi�n: objeto DAO dedicado a cada sesi�n
			sesion.setAttribute("dao", dao);

			logTag.log(Level.INFO, "Objeto DAO generado para la sesi�n con ID: " + sesion.getId());
		}

		return (DAO) sesion.getAttribute("dao");
	}

	/**
	 * Inserta los datos recibidos en la BD.
	 * 
	 * @param request
	 *            Respuesta del usuaio de la app.
	 * @param dao
	 *            Objeto para la comunicacion con BD.
	 * @return
	 */
	private boolean insertar(HttpServletRequest request, DAO dao) {

		boolean realizado;
		Articulo articulo;

		articulo = creaEntidad(request);
		realizado = dao.insertar(articulo);

		return realizado;
	}

	/**
	 * Actualiza los datos pasados por el usuario.
	 * 
	 * @param request
	 * @param dao
	 * @return
	 */

	private boolean actualizar(HttpServletRequest request, DAO dao) {

		boolean realizado;
		Articulo articulo;

		articulo = creaEntidad(request);
		realizado = dao.actualizar(articulo);

		return realizado;

	}

	/**
	 * Elimina el registro de id dado.
	 * 
	 * @param request
	 * @param dao
	 * @return
	 */
	private boolean eliminar(HttpServletRequest request, DAO dao) {

		boolean realizado;
		int id;
		id = Integer.parseInt(request.getParameter("id"));
		realizado = dao.eliminar(id);
		return realizado;

	}

	/**
	 * Crea la entidad que debe manejar el dao.
	 * 
	 * @param request
	 * @param dao
	 * @return
	 */
	private Articulo creaEntidad(HttpServletRequest request) {
		Categoria categoria;
		Articulo articulo;

		categoria = new Categoria(request.getParameter("categoria"));

		articulo = new Articulo(Integer.parseInt(request.getParameter("id")), categoria, request.getParameter("codigo"),
				request.getParameter("nombre"), request.getParameter("descripcion"),
				Double.parseDouble(request.getParameter("existencia")),
				Double.parseDouble(request.getParameter("precio")));

		return articulo;

	}

}
