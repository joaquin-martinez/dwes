﻿	/**
Es necesario modificar esta url y apuntar a la URL del servicio web
*/

var BASE_URL = 'http://localhost:8080/DWEBS_Hibernate';

var app = {

	init: function() {
		app.getItems();
	},

	getItems: function() {
		$.ajax({
			type: 'GET',
			url: BASE_URL + '/tienda',
			dataType: 'json',
			success: function(data){				
				app.listarArticulos(data.articulo);
				app.listarCategorias(data.categorias);
			},
			error: function(error){
				console.log(error);
			}
		});
	}, 
	
	insertar: function() {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/tienda',
			data: { // Paso de parámetros
				action     : "insertar",
				id         : $("#id").val(),
				codigo     : $("#codigo").val(),
				nombre     : $("#nombre").val(),
				descripcion: $("#descripcion").val(),
				existencia : $("#existencia").val(),
				precio     : $("#precio").val(),
				categoria  : $("#categoria").val()
			},
			dataType: 'json',
			success: function(data){
				app.listarArticulos(data.articulo);
				app.listarCategorias(data.categorias);
				
				if (data.success == true)
					alert("Artículo insertado");
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	
	modificar: function() {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/tienda',
			data: { // Paso de parámetros
				action     : "actualizar",
				id         : $("#id").val(),
				codigo     : $("#codigo").val(),
				nombre     : $("#nombre").val(),
				descripcion: $("#descripcion").val(),
				existencia : $("#existencia").val(),
				precio     : $("#precio").val(),
				categoria  : $("#categoria").val()
			},
			dataType: 'json',
			success: function(data){				
				app.listarArticulos(data.articulo);
				app.listarCategorias(data.categorias);
				
				if (data.success == true)
					alert("Artículo modificado");
			},
			error: function(error){
				console.log(error);
			} 
		});
	},
	
	eliminar: function(id) {
		$.ajax({
			type: 'POST',
			url: BASE_URL + '/tienda',
			data: { // Paso de parámetros
				action     : "eliminar",
				id         : id
			},
			dataType: 'json',
			success: function(data){				
				app.listarArticulos(data.articulo);
				app.listarCategorias(data.categorias);
				
				if (data.success == true)
					alert("Artículo eliminado");
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	
	listarCategorias: function(categorias) {		
		// Vaciado de subelementos de las categorias
		$("#categorias").empty();
        
        // Por cada elemento de articulos se representa en el datalist
        $.each(categorias, function(index, value) {
			//console.log(index);
			//console.log(value);
			
			// Elemento del datalist
            if (value.categoria)
            	$("#categorias").append("<option>" + value.categoria +"</option>");
	    });
	},
	
	listarArticulos: function(articulos) {
		// Actualizar la BD LocalStorage
	    var items = localStorage.getItem("items");// Recupera los datos almacenados en la lista	
		localStorage.setItem("items", JSON.stringify(articulos));
		
		// Vaciado de subelementos del listview y categorias
		$("#tblListar").empty();
		
		// Representación: generación de cabecera
		$("#tblListar").html(
		        "<thead>"+
		        "   <tr>"+
		        "   <th>ID</th>"+
		        "   <th>Código</th>"+
		        "   <th>Nombre</th>"+
		        "   <th>Descripción</th>"+
		        "   <th>Existencia</th>"+
		        "   <th>Precio</th>"+
		        "   <th>Categoría</th>"+
		        "   <th></th>"+
		        "   </tr>"+
		        "</thead>"+
		        "<tbody>"+
		        "</tbody>"
		        );
		
		// Por cada elemento de articulos se representa en el listview
		$.each(articulos, function(index, value) {
			//console.log(index);
			//console.log(value);
			
			// Elemento del listview
			$("#tblListar tbody").append("<tr>");
	        $("#tblListar tbody").append("<td>" + value.id + "</td>");
	        $("#tblListar tbody").append("<td>" + value.codigo + "</td>");
	        $("#tblListar tbody").append("<td>" + value.nombre + "</td>");
	        $("#tblListar tbody").append("<td>" + value.descripcion + "</td>");
	        $("#tblListar tbody").append("<td>" + value.existencia + "</td>");
	        $("#tblListar tbody").append("<td>" + value.precio + "</td>");
	        
	        // Chequear existencia de subelemento: categoria
	        if (value.categoria)
	        	$("#tblListar tbody").append("<td>" + value.categoria.categoria +"</td>");
	        else
	        	$("#tblListar tbody").append("<td></td>");
	        
	        $("#tblListar tbody").append("<td><img src='img/edit.png' alt='"+index+"'class='btnEditar' title='editar'/><img src='img/delete.png' alt='"+index+"' title='eliminar' class='btnEliminar'/></td>");
	        $("#tblListar tbody").append("</tr>");
	    });
	}
}

$(document).ready(function() {
    var operacion;
    
	$("#Guardar").on("click", function(){
		if (operacion == "MODIFICAR")
			app.modificar();
		else
			app.insertar();
	});

	$("#tblListar").on("click", ".btnEditar", function(){
		// 1. Recuperar BD Local Storage
		var items = localStorage.getItem("items");// Recupera los datos almacenados en BD LocalStorage
		items = JSON.parse(items); // Convierte string para objeto
		
		// 2. Recuperación posición según el índice seleccionado
		indice_seleccionado = parseInt($(this).attr("alt")); // Obtengo la posición del índice seleccionado del datagrid
		var value = items[indice_seleccionado]; // Obtengo el item seleccionado
		
		// 3. Recupero información en controles
		$("#id").val(value.id);
		$("#codigo").val(value.codigo);
	    $("#nombre").val(value.nombre);
	    $("#descripcion").val(value.descripcion);
	    $("#existencia").val(value.existencia);
	    $("#precio").val(value.precio);
	    
	    // Chequear existencia de subelemento: categoria
        if (value.categoria)
        	$("#categoria").val(value.categoria.categoria);
		
	    // 4. Modificación de atributos y foco principal del cursor
	    $("#id").attr("readonly","readonly");
		$("#codigo").focus();
		
		// 5. Operación predeterminada: MODIFICAR
		operacion = "MODIFICAR";
	});
	
	$("#tblListar").on("click", ".btnEliminar",function(){
		// 1. Recuperar BD Local Storage
		var items = localStorage.getItem("items");// Recupera los datos almacenados en BD LocalStorage
		items = JSON.parse(items); // Convierte string para objeto
		
		// 2. Recuperación posición según el índice seleccionado
		indice_seleccionado = parseInt($(this).attr("alt")); // Obtengo la posición del índice seleccionado del datagrid
		var value = items[indice_seleccionado]; // Obtengo el item seleccionado
		
		// 3. Operación AJAX 
		app.eliminar(value.id);
	});
});