package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RequestMetodos
 */
@WebServlet(
		description = "Muestra algunos metodos de request.", 
		urlPatterns = { 
				"/RequestMetodos", 
				"/s3", 
				"/requestmetodos", 
				"/getinfo"
		}, 
		initParams = { 
				@WebInitParam(name = "appRequestMetodos", value = "Desarrollo de aplicaciones web")
		})
public class RequestMetodos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestMetodos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		InetAddress local = InetAddress.getLocalHost() ;
		
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter() ;
		
		out.println("<p>Sustituya localhost por la ip: " + local.getHostAddress() + " en la URL de la p&aacute;gina y actualice (f5). </p>");
		out.println( "<p>IP servidor: " + request.getLocalAddr() + "</p>");
		out.println( "<p>IP Ciente: " + request.getRemoteAddr() + "</p>");
		out.println( "<p>Puerto servidor: " + request.getServerPort() + "</p>");
		out.println( "<p>Nombre del servidor : " + request.getLocalName() + "</p>");
		out.println( "<p>Metodo : " + request.getMethod() + "</p>");
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
